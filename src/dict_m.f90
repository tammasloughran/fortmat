module dict_m
    implicit none
    private

    type :: char_list_t
        !! A character list type.
        character (len=:), allocatable :: chars
    end type char_list_t

    type, public :: LogicalDict
        !! A simple dictionary container class for the logical data type.
        integer :: length
        type (char_list_t), allocatable :: keys(:)
        logical, allocatable :: vals(:)
        integer :: filled = 0
    contains
        procedure :: get_val
        procedure :: set_key_val_pair
    end type LogicalDict

    interface LogicalDict
        module procedure :: constructor
    end interface

contains

    type (LogicalDict) function constructor(length)
        !! Constructor for LogicalDict.
        integer, intent (in) :: length !! Maximum length of the logical dictionary.

        constructor%length = length
        allocate (constructor%keys(length))
        allocate (constructor%vals(length))
    end function constructor

    function get_val(self, key) result (return_val)
        !! Get a logical value from the dictionary using key
        class (LogicalDict), intent (inout) :: self
        character (len=*) :: key
        logical :: return_val
        integer :: i
        logical :: match

        match = .false.
        do i=1,self%filled
            if (self%keys(i)%chars==key) then
                return_val = self%vals(i)
                match = .true.
            end if
        end do
        if (.not.match) then
            error stop "No key match in dict for key: " // key
        end if
    end function

    subroutine set_key_val_pair(self, key, val)
        !! Place a key value pair in the dictionary.
        !! Will cause an error if the dictionary is full.
        class (LogicalDict), intent (inout) :: self
        character (len=*), intent (in) :: key
        logical, intent (in) :: val

        if (self%filled==self%length) error stop "Cannot set value. Dict is full."
        self%keys(self%filled+1)%chars = key
        self%vals(self%filled+1) = val
        self%filled = self%filled + 1
    end subroutine

end module dict_m

! Copyright (C) 2021  Tammas Loughran
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
