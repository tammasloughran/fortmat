module sourcefile_m
    !! Module sourcefile_m
    !!
    !! Module containing the SourceFile class.
    implicit none
    private

    public :: count_lines

    type, public :: char_list_t
        !! char_list_t is an allocatable character list type. It is intended to be used as an array
        !! for an allocatable list of strings.
        character (len=:), allocatable :: chars
    end type char_list_t

    type, public :: SourceFile
        !! SourceFile is a class containing source code.
        character (len=:), allocatable :: file_name !! Name of file.
        integer :: iounit !! I/O unit number.
        type (char_list_t), allocatable :: lines(:)  !! Array of source code lines.
        integer :: nlines !! Number of lines in the file.
        integer, allocatable :: indent_lvls(:) !! Array of indentation levels.
    contains
        procedure :: load_file
    end type SourceFile

    interface SourceFile
        module procedure init
    end interface

contains

    function count_lines(in_unit) result (n)
        !! Count the number of lines for the in_unit I/O unit and rewind back to the
        !! start of the file.
        integer :: n !! Total number of lines.
        integer :: in_unit !! Input I/O unit.
        integer :: read_stat !! Read status.
        character (len=:), allocatable :: buffer

        rewind (in_unit)
        n = 0
        read_stat = 0
        do while (read_stat==0)
            read (unit=in_unit, fmt='(A)', iostat=read_stat) buffer
            if (read_stat==0) then
                n = n + 1
            end if
        end do
        rewind (in_unit)
    end function count_lines

    type (SourceFile) function init(file_name)
        !! SourceFile constructor.
        character (len=*), intent (in) :: file_name !! Input filename.

        init%file_name = file_name
        open (newunit=init%iounit, file=file_name, status='old', action='readwrite')
        init%nlines = count_lines(init%iounit)
        allocate (init%lines(init%nlines))
        allocate (init%indent_lvls(init%nlines))
        init%indent_lvls = 0
        call init%load_file()
    end function init

    subroutine load_file(self)
        !! Load the file data into the lines attribute.
        class (SourceFile), intent (inout) :: self
        character (len=512) :: buffer
        integer :: i

        do i = 1, self%nlines
            read (unit=self%iounit, fmt='(A)') buffer
            self%lines(i)%chars = buffer
        end do
    end subroutine load_file

end module sourcefile_m

! Copyright (C) 2021  Tammas Loughran
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
