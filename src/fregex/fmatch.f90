module fmatch
    private
    type :: string_t
        character (len=:), allocatable :: s
        integer :: start
        integer :: end_
    end type

    type, public :: Match
        type (string_t), allocatable :: groups(:)
        logical :: matches = .false.
        integer :: num_groups
    contains
        procedure :: group
        procedure :: destroy
    end type

    interface size
        module procedure :: size_
    end interface

    interface Match
        module procedure :: constructor
    end interface

contains

    function constructor(string, vector, num_groups) result (self)
        implicit none
        type (Match) :: self
        character (len=*) :: string
        integer :: vector(0:)
        integer :: i, start, end_, num_groups !, max_size
        allocate (self%groups(0:num_groups-1))

        self%num_groups = num_groups

        do i=0,num_groups-1
            start = vector(2*i) + 1
            end_ = vector(2*i + 1)
            self%groups(i)%s = string(start:end_)
            self%groups(i)%start = start
            self%groups(i)%end_ = end_
            self%matches = .true.
        end do
    end function

    function group(self, group_id) result (buff)
        implicit none
        class (Match) :: self
        character (len=:), allocatable :: buff
        integer :: group_id
        buff = self%groups(group_id)%s
    end function

    function size_(self) result (match_size)
        class (Match) :: self
        match_size = size(self%groups)
    end function

    subroutine destroy(self)
        class (Match) :: self
        deallocate (self%groups)
    end subroutine
end module
