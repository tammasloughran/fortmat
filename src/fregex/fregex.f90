module fregex
    use, intrinsic :: iso_c_binding
    use, intrinsic :: iso_fortran_env
    use pcre_constants
    use fmatch, only: Match
    implicit none
    private

    public :: Match

    type, public :: Regex
        type(c_ptr) :: pcre_ptr
    contains
        procedure :: fullinfo => fregex_fullinfo
        procedure :: compile => fregex_compile
        procedure :: match => fregex_match
        procedure :: substitute => fregex_substitute
    end type

    interface
        function c_pcre_compile( &
                & pattern, &
                & options, &
                & errptr, &
                & erroffset, &
                & tableptr) &
                & result (pcre) bind (C, name="pcre_compile")
            !! PCRE C function binding for c_pcre_compile.
            !! pcre *pcre_compile(const char *pattern, int options, const char **errptr,
            !!         int *erroffset, const unsigned char *tableptr);
            import
            character (len=1, kind=c_char), intent(in) :: pattern(*)
            integer (c_int), intent (in), value :: options
            integer (c_int), intent (out) :: erroffset
            type (c_ptr) :: pcre
            type (c_ptr), intent (inout) :: errptr
            type (c_ptr), value, intent (in) :: tableptr
        end function


        function c_pcre_exec( &
                & code, &
                & extra, &
                & subject, &
                & length, &
                & startoffset, &
                & options, &
                & ovector, &
                & ovecsize) &
                & result (error_code) bind (C, name="pcre_exec")
            !! PCRE C function binding for c_pcre_exec.
            !! int pcre_exec(const pcre*, const pcre_extra*, const char*, int, int, int, int*, int);
            import
            character (len=1, kind=c_char), intent (in) :: subject(*)
            integer (c_int) :: error_code
            integer (c_int), value, intent (in) :: ovecsize
            integer (c_int), dimension (ovecsize), intent (out) :: ovector
            integer (c_int), value, intent (in) :: length
            integer (c_int), value, intent (in) :: options
            integer (c_int), value, intent (in) :: startoffset
            type (c_ptr), value, intent (in) :: code
            type (c_ptr), value, intent (in) :: extra
        end function


        function c_pcre_fullinfo(code, extra, what, pwhere) &
                & result (error_code) bind (C, name="pcre_fullinfo")
            !! PCRE C function binding for c_pcre_fullinfo.
            !! int pcre_fullinfo(const pcre*, const pcre_extra*, int, void*);
            import
            integer (c_int) :: error_code
            integer (c_int), value, intent (in) :: what
            type (c_ptr), intent (out) :: pwhere
            type (c_ptr), value, intent (in) :: code
            type (c_ptr), value, intent (in) :: extra
        end function

    end interface
contains


    subroutine check_pcre_error(error_code)
        !! Check for errors and print usefull informations to standard output.
        integer, intent (in) :: error_code
        if (error_code<0) then
            select case(error_code)
            case (0)
                write (error_unit, *) "Vector Overflow."
                error stop
            case (PCRE_ERROR_NOMATCH)
            case (PCRE_ERROR_NULL)
                write (error_unit, *) "PCRE_PTR is null."
                error stop PCRE_ERROR_NULL
            case (PCRE_ERROR_BADOPTION)
                write (error_unit, *) "Bad option."
                error stop PCRE_ERROR_BADOPTION
            case (PCRE_ERROR_BADMAGIC)
                write (error_unit, *) "Bad magic."
                error stop PCRE_ERROR_BADMAGIC
            case (PCRE_ERROR_UNKNOWN_OPCODE)
                write (error_unit, *) "Unknown OpCode."
                error stop PCRE_ERROR_UNKNOWN_OPCODE
            case default
                write (error_unit, *) "Matching error ", error_code
                error stop
            end select
        end if
    end subroutine


    subroutine fregex_compile(self, pattern, flags)
        !! Fortran side PCRE compile function. COmpile a PCRE expression.
        implicit none
        class (Regex) :: self
        character (len=*), intent (in) :: pattern
        integer (c_int), optional :: flags

        character (len=len(pattern)+1, kind=c_char) :: c_pattern
        type (c_ptr) :: c_error_msg
        integer (c_int) :: error_offset
        integer :: flags_ = PCRE_CONFIG_UTF8

        if (present(flags)) flags_ = flags

        ! C - strings must be NULL terminated
        c_pattern = pattern // c_null_char

        self%pcre_ptr = c_pcre_compile(c_pattern, flags_, c_error_msg, error_offset, c_null_ptr)

        if (.not. c_associated(self%pcre_ptr)) then
            write (error_unit,*) "Unassociated pcre_pointer. Maybe invalid pattern?"
            error stop
        end if
    end subroutine


    function fregex_match(self, string, flags, extra) result (res_match)
        !! Fortran side string matching function. Match a string to a compiled PCRE expression
        !! using the c_pcre_exec function.
        implicit none
        character (len=*) :: string
        class (Regex) :: self
        integer, optional :: flags
        type (Match) :: res_match
        type (c_ptr), optional :: extra

        character (len=len(string)+1, kind=c_char) :: c_string
        integer :: flags_ = PCRE_CONFIG_UTF8
        integer, parameter :: max_size = 30
        integer :: ovector(0:max_size-1)
        integer :: ret_code
        integer :: start_offset = 0
        type (c_ptr) :: extra_ = C_NULL_PTR

        if (present(flags)) flags_ = flags
        if (present(extra)) extra_ = extra

        ! C - Strings must be null terminated
        c_string = string // c_null_char
        ret_code = c_pcre_exec(self%pcre_ptr, extra_, c_string, len(string), start_offset, &
                & flags_, ovector, size(ovector))

        call check_pcre_error(ret_code)
        res_match = Match(string, vector=ovector, num_groups=ret_code)
    end function


    function fregex_substitute(self, subject, replacement, flags, extra) result (out_string)
        !! Substitute the first match on a string 'subject' with the string 'replacement'.
        class (Regex) :: self
        character (len=*), intent (in) :: replacement
        character (len=*), intent (in) :: subject
        character (len=:), allocatable :: out_string
        integer, intent (in), optional :: flags
        type (c_ptr), intent (in), optional :: extra

        character (len=len(subject)+1, kind=c_char) :: c_subject
        integer :: flags_ = PCRE_CONFIG_UTF8
        integer :: i
        integer, parameter :: max_size = 30
        integer :: ovector(0:max_size-1)
        integer :: ret_code
        integer :: start_offset = 0
        type (Match) :: res_match
        type (c_ptr) :: extra_ = C_NULL_PTR

        if (present(flags)) flags_ = flags
        if (present(extra)) extra_ = extra

        ! C - Strings must be null terminated
        c_subject = subject // c_null_char
        ret_code = c_pcre_exec(self%pcre_ptr, extra_, c_subject, len(subject), start_offset, &
                & flags_, ovector, size(ovector))
        call check_pcre_error(ret_code)
        res_match = Match(subject, vector=ovector, num_groups=ret_code)
        !!match = Match(subject, vector=ovector, num_groups=ret_code)
        out_string = subject
        if (res_match%matches) then
            do i=0,res_match%num_groups-1
                out_string = out_string(:res_match%groups(i)%start-1) // replacement // &
                        & out_string(res_match%groups(i)%end_+1:)
            end do
        end if
    end function


    function fregex_fullinfo(self, extra, what, pwhere) result (error_code)
        !! Fortran side PCRE fullinfo function. Calls c_pcre_fullinfo.
        implicit none
        class (Regex) :: self
        integer (c_int) :: error_code
        integer (c_int), intent (in) :: what
        type (c_ptr), intent (in) :: extra
        type (c_ptr), intent (out) :: pwhere
        error_code = c_pcre_fullinfo(self%pcre_ptr, extra, what, pwhere)
    end function
end module
