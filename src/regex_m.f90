module regex_m
    !! Module regex_m
    !!
    !! Contains the regex expressions for formatting.
    implicit none

    character (len=*), parameter :: all_indentables_exp = '^[ \t]*(program|module|submodule|'//&
            &'subroutine|((elemental|real|integer|type|double precision|pure|recursive|impure|'//&
            &'\(\w+\)|complex|character)[ \t])*function |block|if|do|(abstract)*[ \t]*interface'//&
            &'|select|type(?![ \t]*\()|forall|associate|where)'
    character (len=*), parameter :: case_exp = '^[ \t]*(case[ \t]*(\())'
    character (len=*), parameter :: classis_exp = '^[ \t]*(class[ \t]*is)'
    character (len=*), parameter :: cmnt_exp = '("[^"]*".*)\K(''[^'']*''.*)\K!.*'
    character (len=*), parameter :: contains_exp = '^[ \t]*contains'
    character (len=*), parameter :: default_class_exp = '^[ \t]*(default[ \t]class)'
    character (len=*), parameter :: else_exp = '^[ \t]*(else[ \t]*(if)*)'
    character (len=*), parameter :: elsewhere_exp = '^[ \t]*elsewhere\s*(?![\w\d])'
    character (len=*), parameter :: end_all_indentables_exp = '^[ \t]*end\s*(program|module|'//&
            &'submodule|subroutine|function|block|if|do|interface|select|type|forall|'//&
            &'associate|where)'
    character (len=*), parameter :: end_associate_exp = '^[ \t]*(end *associate)'
    character (len=*), parameter :: simple_end_associate_exp = 'end[ \t]*associate'
    character (len=*), parameter :: end_block_exp = '^[ \t]*(end *block)'
    character (len=*), parameter :: simple_end_block_exp = 'end[ \t]*block'
    character (len=*), parameter :: end_do_exp = '^[ \t]*end *do'
    character (len=*), parameter :: end_exp = '^[ \t]*end[ \t]*(?![\w\d \t])'
    character (len=*), parameter :: end_forall_exp = '^[ \t]*\d*[ \t]*(\w*:)?[ \t]*(end *forall)'
    character (len=*), parameter :: simple_end_forall_exp = 'end[ \t]*forall'
    character (len=*), parameter :: end_function_exp = '^[ \t]*end *function'
    character (len=*), parameter :: end_if_exp = '^[ \t]*end *if'
    character (len=*), parameter :: end_interface_exp = '^[ \t]*(end *interface)'
    character (len=*), parameter :: simple_end_interface_exp = 'end[ \t]*interface'
    character (len=*), parameter :: end_module_exp = '^[ \t]*end *module'
    character (len=*), parameter :: end_program_exp = '^[ \t]*end *program'
    character (len=*), parameter :: end_select_exp = '^[ \t]*(end *select)'
    character (len=*), parameter :: simple_end_select_exp = 'end[ \t]*select'
    character (len=*), parameter :: end_submodule_exp = '^[ \t]*end *submodule'
    character (len=*), parameter :: end_subroutine_exp = '^[ \t]*end *subroutine'
    character (len=*), parameter :: end_type_exp = '^[ \t]*(end *type)'
    character (len=*), parameter :: end_where_exp = '^[ \t]*end *where'
    character (len=*), parameter :: line_continue_exp = '&[ \t]*$'
    character (len=*), parameter :: start_associate_exp = '^[ \t]*\d*[ \t]*(\w*:)?[ \t]*(associate )'
    character (len=*), parameter :: start_block_exp = '^[ \t]*\d*[ \t]*(\w*:)?[ \t]*(block)'
    character (len=*), parameter :: start_do_exp = '^[ \t]*\d*[ \t]*(\w*:)?[ \t]*do'
    character (len=*), parameter :: start_forall_exp = '^[ \t]*\d*[ \t]*(\w*:)?[ \t]*(forall )'
    character (len=*), parameter :: start_function_exp = '^[ \t]*((elemental|real|integer|type|'//&
            &'double precision|pure|recursive|impure|\(\w+\)|complex|character)[ \t])*function'
    character (len=*), parameter :: start_if_exp = '^[ \t]*\d*[ \t]*(\w*:)?[ \t]*if\s*.*\)\s*then*(?![\w])'
    character (len=*), parameter :: start_interface_exp = '^[ \t]*(abstract)?[ \t]*(interface)'
    character (len=*), parameter :: start_module_exp = '^[ \t]*(module)(?! procedure)'
    character (len=*), parameter :: start_program_exp = '^[ \t]*program '
    character (len=*), parameter :: start_select_exp = '^[ \t]*\d*[ \t]*(\w*:)?[ \t]*(select )'
    character (len=*), parameter :: start_submodule_exp = '^[ \t]*(submodule)'
    character (len=*), parameter :: start_subroutine_exp = '^[ \t]*subroutine '
    character (len=*), parameter :: start_type_exp = '^[ \t]*(type)(?![ \t]*\()'
    character (len=*), parameter :: start_where_exp = '^[ \t]*\d*[ \t]*(\w*:)?[ \t]*where\s*\('
    character (len=*), parameter :: typeis_exp = '^[ \t]*(type[ \t]*is)'

    character (len=*), parameter :: associate_par_exp = 'associate[ \t]*\('
    character (len=*), parameter :: case_par_exp = 'case[ \t]*\('
    character (len=*), parameter :: character_par_exp = 'character[ \t]*\('
    character (len=*), parameter :: close_par_exp = 'close[ \t]*\('
    character (len=*), parameter :: complex_par_exp = 'complex[ \t]*\('
    character (len=*), parameter :: forall_par_exp = 'forall[ \t]*\('
    character (len=*), parameter :: if_par_exp = 'if[ \t]*\('
    character (len=*), parameter :: integer_par_exp = 'integer[ \t]*\('
    character (len=*), parameter :: intent_par_exp = 'intent[ \t]*\('
    character (len=*), parameter :: classis_par_exp = 'class[ \t]*is[ \t]*\('
    character (len=*), parameter :: typeis_par_exp = 'type[ \t]*is[ \t]*\('
    character (len=*), parameter :: logical_par_exp = 'logical[ \t]*\('
    character (len=*), parameter :: open_par_exp = 'open[ \t]*\('
    character (len=*), parameter :: real_par_exp = 'real[ \t]*\('
    character (len=*), parameter :: rewind_par_exp = 'rewind[ \t]*\('
    character (len=*), parameter :: type_par_exp = 'type[ \t]*\('
    character (len=*), parameter :: where_par_exp = 'where[ \t]*\('
    character (len=*), parameter :: write_par_exp = 'write[ \t]*\('
    character (len=*), parameter :: separator_exp = '[\ t]*::[ \t]*'
    character (len=*), parameter :: class_par_exp = 'class[ \t]*\('
    character (len=*), parameter :: dimension_par_exp = 'dimension[ \t]*\('
    character (len=*), parameter :: do_while_exp = 'do[ \t]*while'
    character (len=*), parameter :: dowhile_par_exp = 'do[ \t]*while[ \t]*\('
    character (len=*), parameter :: else_if_exp = 'else[ \t]*if'
    character (len=*), parameter :: elseif_par_exp = 'else[ \t]*if[ \t]*\('
    character (len=*), parameter :: allocate_par_exp = 'allocate[ \t]*\('
    character (len=*), parameter :: deallocate_par_exp = 'deallocate[ \t]*\('
    character (len=*), parameter :: par_then_exp = '\)[ \t]*then'
    character (len=*), parameter :: print_exp = 'print[ \t]*'
    character (len=*), parameter :: result_par_exp = 'result[ \t]*\('

end module regex_m

! Copyright (C) 2021  Tammas Loughran
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
