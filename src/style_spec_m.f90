module style_spec_m
    !! ## Module style_spec_m
    !!
    !! Module to read namelist of style specification and costruct the style spec data type.
    use cli_m, only: style_spec_file, output_spec
    use dict_m, only: LogicalDict
    implicit none

    type :: style_spec_t
        !! Style specification type.
        integer :: indent_width !! Width of indentation.
        integer :: num_hspace_allocate_par !! Number of whitespace between allocate and (
        integer :: num_hspace_associate_par !! Number of whitespace between associate and (
        integer :: num_hspace_case_par !! Number of whitespace between case and (
        integer :: num_hspace_character_par !! Number of whitespace between character and (
        integer :: num_hspace_class_par !! Number of whitespace between class and (
        integer :: num_hspace_classis_par !! Number of whitespace between classis and (
        integer :: num_hspace_close_par !! Number of whitespace between close and (
        integer :: num_hspace_complex_par !! Number of whitespace between complex and (
        integer :: num_hspace_deallocate_par !! Number of whitespace between deallocate and (
        integer :: num_hspace_dimension_par !! Number of whitespace between dimension and (
        integer :: num_hspace_do_while !! Number of whitespace between do and while
        integer :: num_hspace_dowhile_par !! Number of whitespace between dowhile and (
        integer :: num_hspace_else_if !! Number of whitespace between else and if
        integer :: num_hspace_elseif_par !! Number of whitespace between elseif and (
        integer :: num_hspace_end_associate !! Number of whitespace between end and associate
        integer :: num_hspace_end_block !! Number of whitespace between end and block
        integer :: num_hspace_end_do !! Number of whitespace between end and do
        integer :: num_hspace_end_forall !! Number of whitespace between end and forall
        integer :: num_hspace_end_function !! Number of whitespace between end and function
        integer :: num_hspace_end_if !! Number of whitespace between end and if
        integer :: num_hspace_end_interface !! Number of whitespace between end and interface
        integer :: num_hspace_end_module !! Number of whitespace between end and module
        integer :: num_hspace_end_program !! Number of whitespace between end and program
        integer :: num_hspace_end_select !! Number of whitespace between end and select
        integer :: num_hspace_end_submodule !! Number of whitespace between end and submodule
        integer :: num_hspace_end_subroutine !! Number of whitespace between end and subroutine
        integer :: num_hspace_end_where !! Number of whitespace between end and where
        integer :: num_hspace_forall_par !! Number of whitespace between forall and (
        integer :: num_hspace_if_par !! Number of whitespace between if and (
        integer :: num_hspace_integer_par !! Number of whitespace between integer and (
        integer :: num_hspace_intent_par !! Number of whitespace between intent and (
        integer :: num_hspace_logical_par !! Number of whitespace between logical and (
        integer :: num_hspace_open_par !! Number of whitespace between open and (
        integer :: num_hspace_par_then !! Number of whitespace between ) and then
        integer :: num_hspace_print_star !! Number of whitespace between print and *
        integer :: num_hspace_real_par !! Number of whitespace between real and (
        integer :: num_hspace_result_par !! Number of whitespace between result and (
        integer :: num_hspace_rewind_par !! Number of whitespace between rewind and (
        integer :: num_hspace_type_par !! Number of whitespace between type and (
        integer :: num_hspace_typeis_par !! Number of whitespace between integer and (
        integer :: num_hspace_where_par !! Number of whitespace between where and (
        integer :: num_hspace_write_par !! Number of whitespaces between write and (
        integer :: num_hspace_separator !! Number of whitespaces surrounding ::
        integer :: num_of_continue_indents !! Number of indent levels for continued lines.
        integer :: num_rules = 66 !! Total number of rules that can be executed.

        type (LogicalDict) :: options !! List of which rules need to be run.
    end type style_spec_t

    type (style_spec_t) :: style

contains

    subroutine construct_style_spec()
        !! Construct the style specification from a namelist file
        integer :: nml_unit
        integer :: indent_width, num_of_continue_indents
        integer :: num_hspace_associate_par, &
                num_hspace_allocate_par, &
                num_hspace_case_par, &
                num_hspace_character_par, &
                num_hspace_class_par, &
                num_hspace_classis_par, &
                num_hspace_close_par, &
                num_hspace_complex_par, &
                num_hspace_deallocate_par, &
                num_hspace_dimension_par, &
                num_hspace_do_while, &
                num_hspace_dowhile_par, &
                num_hspace_else_if, &
                num_hspace_elseif_par, &
                num_hspace_end_associate, &
                num_hspace_end_block, &
                num_hspace_end_do, &
                num_hspace_end_forall, &
                num_hspace_end_function, &
                num_hspace_end_if, &
                num_hspace_end_interface, &
                num_hspace_end_module, &
                num_hspace_end_program, &
                num_hspace_end_select, &
                num_hspace_end_submodule, &
                num_hspace_end_subroutine, &
                num_hspace_end_where, &
                num_hspace_forall_par, &
                num_hspace_if_par, &
                num_hspace_integer_par, &
                num_hspace_intent_par, &
                num_hspace_logical_par, &
                num_hspace_open_par, &
                num_hspace_par_then, &
                num_hspace_print_star, &
                num_hspace_real_par, &
                num_hspace_result_par, &
                num_hspace_rewind_par, &
                num_hspace_separator, &
                num_hspace_type_par, &
                num_hspace_typeis_par, &
                num_hspace_where_par, &
                num_hspace_write_par
        logical :: hspace_allocate_par, &
                hspace_associate_par, &
                hspace_case_par, &
                hspace_character_par, &
                hspace_classis_par, &
                hspace_class_par, &
                hspace_close_par, &
                hspace_complex_par, &
                hspace_deallocate_par, &
                hspace_dimension_par, &
                hspace_do_while, &
                hspace_dowhile_par, &
                hspace_else_if, &
                hspace_elseif_par, &
                hspace_end_associate, &
                hspace_end_block, &
                hspace_end_do, &
                hspace_end_forall, &
                hspace_end_function, &
                hspace_end_if, &
                hspace_end_interface, &
                hspace_end_module, &
                hspace_end_program, &
                hspace_end_select, &
                hspace_end_submodule, &
                hspace_end_subroutine, &
                hspace_end_where, &
                hspace_forall_par, &
                hspace_if_par, &
                hspace_integer_par, &
                hspace_intent_par, &
                hspace_logical_par, &
                hspace_open_par, &
                hspace_par_then, &
                hspace_print_star, &
                hspace_real_par, &
                hspace_result_par, &
                hspace_rewind_par, &
                hspace_separator, &
                hspace_type_par, &
                hspace_typeis_par, &
                hspace_where_par, &
                hspace_write_par, &
                indent_associate, &
                indent_block, &
                indent_case_clause, &
                indent_classis_clause, &
                indent_continued_line, &
                indent_default_class_clause, &
                indent_do, &
                indent_forall, &
                indent_function, &
                indent_if, &
                indent_interface, &
                indent_module, &
                indent_prog, &
                indent_select, &
                indent_submodule, &
                indent_subroutine, &
                indent_type, &
                indent_typeis_clause, &
                indent_where, &
                unindent_case, &
                unindent_contains, &
                unindent_else, &
                unindent_elsewhere
        logical :: nml_file_exists
        namelist /indent_spec/ &
                indent_associate, &
                indent_block, &
                indent_case_clause, &
                indent_classis_clause, &
                indent_continued_line, &
                indent_default_class_clause, &
                indent_do, &
                indent_forall, &
                indent_function, &
                indent_if, &
                indent_interface, &
                indent_module, &
                indent_prog, &
                indent_select, &
                indent_submodule, &
                indent_subroutine, &
                indent_type, &
                indent_typeis_clause, &
                indent_where, &
                indent_width, &
                num_of_continue_indents, &
                unindent_case, &
                unindent_contains, &
                unindent_else, &
                unindent_elsewhere
        namelist /hspace_spec/ &
                hspace_allocate_par, &
                hspace_associate_par, &
                hspace_case_par, &
                hspace_character_par, &
                hspace_class_par, &
                hspace_classis_par, &
                hspace_close_par, &
                hspace_complex_par, &
                hspace_deallocate_par, &
                hspace_dimension_par, &
                hspace_do_while, &
                hspace_dowhile_par, &
                hspace_else_if, &
                hspace_elseif_par, &
                hspace_end_associate, &
                hspace_end_block, &
                hspace_end_do, &
                hspace_end_forall, &
                hspace_end_function, &
                hspace_end_if, &
                hspace_end_interface, &
                hspace_end_module, &
                hspace_end_program, &
                hspace_end_select, &
                hspace_end_submodule, &
                hspace_end_subroutine, &
                hspace_end_where, &
                hspace_forall_par, &
                hspace_if_par, &
                hspace_integer_par, &
                hspace_intent_par, &
                hspace_logical_par, &
                hspace_open_par, &
                hspace_par_then, &
                hspace_print_star, &
                hspace_real_par, &
                hspace_result_par, &
                hspace_rewind_par, &
                hspace_separator, &
                hspace_type_par, &
                hspace_typeis_par, &
                hspace_where_par, &
                hspace_write_par, &
                num_hspace_allocate_par, &
                num_hspace_associate_par, &
                num_hspace_case_par, &
                num_hspace_character_par, &
                num_hspace_classis_par, &
                num_hspace_class_par, &
                num_hspace_close_par, &
                num_hspace_complex_par, &
                num_hspace_deallocate_par, &
                num_hspace_do_while, &
                num_hspace_dowhile_par, &
                num_hspace_else_if, &
                num_hspace_elseif_par, &
                num_hspace_end_associate, &
                num_hspace_end_block, &
                num_hspace_end_do, &
                num_hspace_end_forall, &
                num_hspace_end_function, &
                num_hspace_end_if, &
                num_hspace_end_interface, &
                num_hspace_end_module, &
                num_hspace_end_program, &
                num_hspace_end_select, &
                num_hspace_end_submodule, &
                num_hspace_end_subroutine, &
                num_hspace_end_where, &
                num_hspace_forall_par, &
                num_hspace_if_par, &
                num_hspace_integer_par, &
                num_hspace_intent_par, &
                num_hspace_logical_par, &
                num_hspace_open_par, &
                num_hspace_par_then, &
                num_hspace_print_star, &
                num_hspace_real_par, &
                num_hspace_result_par, &
                num_hspace_rewind_par, &
                num_hspace_separator, &
                num_hspace_type_par, &
                num_hspace_typeis_par, &
                num_hspace_where_par, &
                num_hspace_write_par

        ! Set default specification
        hspace_allocate_par = .true.
        hspace_associate_par = .true.
        hspace_case_par = .true.
        hspace_character_par = .true.
        hspace_classis_par = .true.
        hspace_class_par = .true.
        hspace_close_par = .true.
        hspace_complex_par = .true.
        hspace_deallocate_par = .true.
        hspace_dimension_par = .true.
        hspace_do_while = .true.
        hspace_dowhile_par = .true.
        hspace_else_if = .true.
        hspace_elseif_par = .true.
        hspace_end_associate = .true.
        hspace_end_block = .true.
        hspace_end_do = .true.
        hspace_end_forall = .true.
        hspace_end_function = .true.
        hspace_end_if = .true.
        hspace_end_interface = .true.
        hspace_end_module = .true.
        hspace_end_program = .true.
        hspace_end_select = .true.
        hspace_end_submodule = .true.
        hspace_end_subroutine = .true.
        hspace_end_where = .true.
        hspace_forall_par = .true.
        hspace_if_par = .true.
        hspace_integer_par = .true.
        hspace_intent_par = .true.
        hspace_logical_par = .true.
        hspace_open_par = .true.
        hspace_par_then = .true.
        hspace_print_star = .true.
        hspace_real_par = .true.
        hspace_result_par = .true.
        hspace_rewind_par = .true.
        hspace_separator = .true.
        hspace_type_par = .true.
        hspace_typeis_par = .true.
        hspace_where_par = .true.
        hspace_write_par = .true.
        indent_associate = .true.
        indent_block = .true.
        indent_case_clause = .true.
        indent_classis_clause = .true.
        indent_continued_line = .true.
        indent_default_class_clause = .true.
        indent_do = .true.
        indent_forall = .true.
        indent_function = .true.
        indent_if = .true.
        indent_interface = .true.
        indent_module = .true.
        indent_prog = .true.
        indent_select = .true.
        indent_submodule = .true.
        indent_subroutine = .true.
        indent_type = .true.
        indent_typeis_clause = .true.
        indent_where = .true.
        indent_width = 4
        num_hspace_allocate_par = 1
        num_hspace_associate_par = 1
        num_hspace_associate_par = 1
        num_hspace_case_par = 1
        num_hspace_character_par = 1
        num_hspace_classis_par = 1
        num_hspace_class_par = 1
        num_hspace_close_par = 1
        num_hspace_complex_par = 1
        num_hspace_deallocate_par = 1
        num_hspace_dimension_par = 1
        num_hspace_do_while = 1
        num_hspace_dowhile_par = 1
        num_hspace_else_if = 1
        num_hspace_elseif_par = 1
        num_hspace_end_associate = 1
        num_hspace_end_block = 1
        num_hspace_end_do = 1
        num_hspace_end_forall = 1
        num_hspace_end_function = 1
        num_hspace_end_if = 1
        num_hspace_end_interface = 1
        num_hspace_end_module = 1
        num_hspace_end_program = 1
        num_hspace_end_select = 1
        num_hspace_end_submodule = 1
        num_hspace_end_subroutine = 1
        num_hspace_end_where = 1
        num_hspace_forall_par = 1
        num_hspace_if_par = 1
        num_hspace_integer_par = 1
        num_hspace_intent_par = 1
        num_hspace_logical_par = 1
        num_hspace_open_par = 1
        num_hspace_par_then = 1
        num_hspace_print_star = 1
        num_hspace_real_par = 1
        num_hspace_result_par = 1
        num_hspace_rewind_par = 1
        num_hspace_separator = 1
        num_hspace_type_par = 1
        num_hspace_typeis_par = 1
        num_hspace_where_par = 1
        num_hspace_write_par = 1
        num_of_continue_indents = 2
        unindent_case = .true.
        unindent_contains = .true.
        unindent_else = .true.
        unindent_elsewhere = .true.

        ! Read namelist
        inquire (file=style_spec_file, exist=nml_file_exists)
        if (nml_file_exists) then
            open (newunit=nml_unit, file=style_spec_file, status='old', action='read')
            read (unit=nml_unit, nml=indent_spec)
        else if (output_spec .and. .not. nml_file_exists) then
            open (newunit=nml_unit, file='default_spec.nml', status='unknown', action='write')
            write (unit=nml_unit, nml=indent_spec)
            write (unit=nml_unit, nml=hspace_spec)
            close (unit=nml_unit)
        end if

        ! Add specification options to style object
        style%indent_width = indent_width
        style%num_hspace_allocate_par = num_hspace_allocate_par
        style%num_hspace_associate_par = num_hspace_associate_par
        style%num_hspace_case_par = num_hspace_case_par
        style%num_hspace_character_par = num_hspace_character_par
        style%num_hspace_class_par = num_hspace_class_par
        style%num_hspace_classis_par = num_hspace_classis_par
        style%num_hspace_close_par = num_hspace_close_par
        style%num_hspace_complex_par = num_hspace_complex_par
        style%num_hspace_deallocate_par = num_hspace_deallocate_par
        style%num_hspace_dimension_par = num_hspace_dimension_par
        style%num_hspace_dimension_par = num_hspace_dimension_par
        style%num_hspace_do_while = num_hspace_do_while
        style%num_hspace_dowhile_par = num_hspace_dowhile_par
        style%num_hspace_else_if = num_hspace_else_if
        style%num_hspace_elseif_par = num_hspace_elseif_par
        style%num_hspace_end_associate = num_hspace_end_associate
        style%num_hspace_end_block = num_hspace_end_block
        style%num_hspace_end_do = num_hspace_end_do
        style%num_hspace_end_forall = num_hspace_end_forall
        style%num_hspace_end_function = num_hspace_end_function
        style%num_hspace_end_if = num_hspace_end_if
        style%num_hspace_end_interface = num_hspace_end_interface
        style%num_hspace_end_module = num_hspace_end_module
        style%num_hspace_end_program = num_hspace_end_program
        style%num_hspace_end_select = num_hspace_end_select
        style%num_hspace_end_submodule = num_hspace_end_submodule
        style%num_hspace_end_subroutine = num_hspace_end_subroutine
        style%num_hspace_end_where = num_hspace_end_where
        style%num_hspace_forall_par = num_hspace_forall_par
        style%num_hspace_if_par = num_hspace_if_par
        style%num_hspace_integer_par = num_hspace_integer_par
        style%num_hspace_intent_par = num_hspace_intent_par
        style%num_hspace_logical_par = num_hspace_logical_par
        style%num_hspace_open_par = num_hspace_open_par
        style%num_hspace_par_then = num_hspace_par_then
        style%num_hspace_print_star = num_hspace_print_star
        style%num_hspace_real_par = num_hspace_real_par
        style%num_hspace_result_par = num_hspace_result_par
        style%num_hspace_rewind_par = num_hspace_rewind_par
        style%num_hspace_separator = num_hspace_separator
        style%num_hspace_type_par = num_hspace_type_par
        style%num_hspace_typeis_par = num_hspace_typeis_par
        style%num_hspace_where_par = num_hspace_where_par
        style%num_hspace_write_par = num_hspace_write_par
        style%num_of_continue_indents = num_of_continue_indents
        style%options = LogicalDict(style%num_rules)
        call style%options%set_key_val_pair('hspace_allocate_par', hspace_allocate_par)
        call style%options%set_key_val_pair('hspace_associate_par', hspace_associate_par)
        call style%options%set_key_val_pair('hspace_case_par', hspace_case_par)
        call style%options%set_key_val_pair('hspace_character_par', hspace_character_par)
        call style%options%set_key_val_pair('hspace_class_par', hspace_class_par)
        call style%options%set_key_val_pair('hspace_classis_par', hspace_classis_par)
        call style%options%set_key_val_pair('hspace_close_par', hspace_close_par)
        call style%options%set_key_val_pair('hspace_complex_par', hspace_complex_par)
        call style%options%set_key_val_pair('hspace_deallocate_par', hspace_deallocate_par)
        call style%options%set_key_val_pair('hspace_dimension_par', hspace_dimension_par)
        call style%options%set_key_val_pair('hspace_do_while', hspace_do_while)
        call style%options%set_key_val_pair('hspace_dowhile_par', hspace_dowhile_par)
        call style%options%set_key_val_pair('hspace_else_if', hspace_else_if)
        call style%options%set_key_val_pair('hspace_elseif_par', hspace_elseif_par)
        call style%options%set_key_val_pair('hspace_end_associate', hspace_end_associate)
        call style%options%set_key_val_pair('hspace_end_block', hspace_end_block)
        call style%options%set_key_val_pair('hspace_end_do', hspace_end_do)
        call style%options%set_key_val_pair('hspace_end_forall', hspace_end_forall)
        call style%options%set_key_val_pair('hspace_end_function', hspace_end_function)
        call style%options%set_key_val_pair('hspace_end_if', hspace_end_if)
        call style%options%set_key_val_pair('hspace_end_interface', hspace_end_interface)
        call style%options%set_key_val_pair('hspace_end_module', hspace_end_module)
        call style%options%set_key_val_pair('hspace_end_program', hspace_end_program)
        call style%options%set_key_val_pair('hspace_end_select', hspace_end_select)
        call style%options%set_key_val_pair('hspace_end_submodule', hspace_end_submodule)
        call style%options%set_key_val_pair('hspace_end_subroutine', hspace_end_subroutine)
        call style%options%set_key_val_pair('hspace_end_where', hspace_end_block)
        call style%options%set_key_val_pair('hspace_forall_par', hspace_forall_par)
        call style%options%set_key_val_pair('hspace_if_par', hspace_if_par)
        call style%options%set_key_val_pair('hspace_integer_par', hspace_integer_par)
        call style%options%set_key_val_pair('hspace_intent_par', hspace_intent_par)
        call style%options%set_key_val_pair('hspace_logical_par', hspace_logical_par)
        call style%options%set_key_val_pair('hspace_open_par', hspace_open_par)
        call style%options%set_key_val_pair('hspace_par_then', hspace_par_then)
        call style%options%set_key_val_pair('hspace_print_star', hspace_print_star)
        call style%options%set_key_val_pair('hspace_real_par', hspace_real_par)
        call style%options%set_key_val_pair('hspace_result_par', hspace_result_par)
        call style%options%set_key_val_pair('hspace_rewind_par', hspace_rewind_par)
        call style%options%set_key_val_pair('hspace_separator', hspace_separator)
        call style%options%set_key_val_pair('hspace_type_par', hspace_type_par)
        call style%options%set_key_val_pair('hspace_typeis_par', hspace_typeis_par)
        call style%options%set_key_val_pair('hspace_where_par', hspace_where_par)
        call style%options%set_key_val_pair('hspace_write_par', hspace_write_par)
        call style%options%set_key_val_pair('indent_associate', indent_associate)
        call style%options%set_key_val_pair('indent_block', indent_block)
        call style%options%set_key_val_pair('indent_case_clause', indent_case_clause)
        call style%options%set_key_val_pair('indent_classis_clause', indent_classis_clause)
        call style%options%set_key_val_pair('indent_continued_line', indent_continued_line)
        call style%options%set_key_val_pair('indent_default_class_clause', indent_default_class_clause)
        call style%options%set_key_val_pair('indent_do', indent_do)
        call style%options%set_key_val_pair('indent_forall', indent_forall)
        call style%options%set_key_val_pair('indent_function', indent_function)
        call style%options%set_key_val_pair('indent_if', indent_if)
        call style%options%set_key_val_pair('indent_interface', indent_interface)
        call style%options%set_key_val_pair('indent_module', indent_module)
        call style%options%set_key_val_pair('indent_prog', indent_prog)
        call style%options%set_key_val_pair('indent_select', indent_select)
        call style%options%set_key_val_pair('indent_submodule', indent_submodule)
        call style%options%set_key_val_pair('indent_subroutine', indent_subroutine)
        call style%options%set_key_val_pair('indent_type', indent_type)
        call style%options%set_key_val_pair('indent_typeis_clause', indent_typeis_clause)
        call style%options%set_key_val_pair('indent_where', indent_where)
        call style%options%set_key_val_pair('unindent_case', unindent_case)
        call style%options%set_key_val_pair('unindent_contains', unindent_contains)
        call style%options%set_key_val_pair('unindent_else', unindent_else)
        call style%options%set_key_val_pair('unindent_elsewhere', unindent_elsewhere)
    end subroutine construct_style_spec

end module style_spec_m

! Copyright (C) 2021  Tammas Loughran
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
