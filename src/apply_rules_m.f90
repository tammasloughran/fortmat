module apply_rules_m
    !! # Module apply_rules_m
    !!
    !! This module applies rules to the source files.
    use sourcefile_m, only: SourceFile
    implicit none

contains

    subroutine apply_indents(source)
        !! Indent the source file to the final indent_lvls. This is the final step of indent rules.
        use style_spec_m, only: style
        !! Indent lines to current indentation level.
        type (SourceFile), intent (inout) :: source !! Source file.
        character (len=:), allocatable :: one_indent ! Size of one indent level.
        integer :: i

        allocate (character (len=style%indent_width) :: one_indent)
        one_indent = repeat(' ', style%indent_width)
        do i=1,source%nlines
            source%lines(i)%chars = trim( &
                    repeat(one_indent, source%indent_lvls(i)) // &
                    adjustl(source%lines(i)%chars))
        end do
    end subroutine apply_indents

    subroutine apply_rules_to_file(rules_to_run, source)
        !! Execute the rules list on a source file. This subroutine may execute the rules
        !! subroutines in rules_m.
        use rules_m, only: rule_list_t
        !! Apply rules to a file.
        type (rule_list_t), intent(in) :: rules_to_run(:) !! Array of rules to run
        type (SourceFile), intent (inout) :: source !! Source file
        integer :: i, nrules

        nrules = size(rules_to_run)
        do i=1,size(rules_to_run)
            call rules_to_run(i)%routine(source)
        end do
        call apply_indents(source)
    end subroutine apply_rules_to_file

end module apply_rules_m

! Copyright (C) 2021  Tammas Loughran
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
