! TODO: add statement spacings
!       - space options
!       - operations and terms
!       - do loop triplets
!       - functions and args
! TODO: vertical spacing
module rules_m
    !! Module that contains formatting rules.
    use fregex, only: Regex, Match
    use sourcefile_m, only: SourceFile, char_list_t
    implicit none

    type :: rule_list_t
        !! A type for an array list of rule procedures.
        procedure (rule_procedure), pointer, nopass :: routine
    end type rule_list_t

    abstract interface
        subroutine rule_procedure(source)
            !! Interface for rule procedures.
            import
            type (SourceFile), intent (inout) :: source
        end subroutine rule_procedure
    end interface

contains

    function construct_rules_list() result (rules_to_run)
        !! Construct the rules_list_t to execute on the file.
        use style_spec_m, only: style
        integer :: i, j
        integer :: num_rules_to_run
        type (rule_list_t) :: rule_list(style%num_rules)
        type (rule_list_t), allocatable :: rules_to_run(:)
        type (char_list_t) :: rule_names(style%num_rules)

        ! Create the ordered list of rules. Make sure rule_name and rule_list have the same order.
        rule_list(1)%routine => hspace_allocate_par
        rule_list(2)%routine => hspace_associate_par
        rule_list(3)%routine => hspace_case_par
        rule_list(4)%routine => hspace_character_par
        rule_list(5)%routine => hspace_class_par
        rule_list(6)%routine => hspace_classis_par
        rule_list(7)%routine => hspace_close_par
        rule_list(8)%routine => hspace_complex_par
        rule_list(9)%routine => hspace_deallocate_par
        rule_list(10)%routine => hspace_dimension_par
        rule_list(11)%routine => hspace_do_while
        rule_list(12)%routine => hspace_dowhile_par
        rule_list(13)%routine => hspace_else_if
        rule_list(14)%routine => hspace_elseif_par
        rule_list(15)%routine => hspace_end_associate
        rule_list(16)%routine => hspace_end_block
        rule_list(17)%routine => hspace_end_do
        rule_list(18)%routine => hspace_end_forall
        rule_list(19)%routine => hspace_end_function
        rule_list(20)%routine => hspace_end_if
        rule_list(21)%routine => hspace_end_interface
        rule_list(22)%routine => hspace_end_module
        rule_list(23)%routine => hspace_end_program
        rule_list(24)%routine => hspace_end_select
        rule_list(25)%routine => hspace_end_submodule
        rule_list(26)%routine => hspace_end_subroutine
        rule_list(27)%routine => hspace_end_where
        rule_list(28)%routine => hspace_forall_par
        rule_list(29)%routine => hspace_if_par
        rule_list(30)%routine => hspace_integer_par
        rule_list(31)%routine => hspace_intent_par
        rule_list(32)%routine => hspace_logical_par
        rule_list(33)%routine => hspace_open_par
        rule_list(34)%routine => hspace_par_then
        rule_list(35)%routine => hspace_print_star
        rule_list(36)%routine => hspace_real_par
        rule_list(37)%routine => hspace_result_par
        rule_list(38)%routine => hspace_rewind_par
        rule_list(39)%routine => hspace_separator
        rule_list(40)%routine => hspace_type_par
        rule_list(41)%routine => hspace_typeis_par
        rule_list(42)%routine => hspace_where_par
        rule_list(43)%routine => hspace_write_par
        rule_list(44)%routine => indent_associate
        rule_list(45)%routine => indent_block
        rule_list(46)%routine => indent_case_clause
        rule_list(47)%routine => indent_classis_clause
        rule_list(48)%routine => indent_continued_line
        rule_list(49)%routine => indent_default_class_clause
        rule_list(50)%routine => indent_do
        rule_list(51)%routine => indent_forall
        rule_list(52)%routine => indent_function
        rule_list(53)%routine => indent_if
        rule_list(54)%routine => indent_interface
        rule_list(55)%routine => indent_module
        rule_list(56)%routine => indent_prog
        rule_list(57)%routine => indent_select
        rule_list(58)%routine => indent_submodule
        rule_list(59)%routine => indent_subroutine
        rule_list(60)%routine => indent_type
        rule_list(61)%routine => indent_typeis_clause
        rule_list(62)%routine => indent_where
        rule_list(63)%routine => unindent_case
        rule_list(64)%routine => unindent_contains
        rule_list(65)%routine => unindent_else
        rule_list(66)%routine => unindent_elsewhere

        rule_names(1)%chars = 'hspace_allocate_par'
        rule_names(2)%chars = 'hspace_associate_par'
        rule_names(3)%chars = 'hspace_case_par'
        rule_names(4)%chars = 'hspace_character_par'
        rule_names(5)%chars = 'hspace_class_par'
        rule_names(6)%chars = 'hspace_classis_par'
        rule_names(7)%chars = 'hspace_close_par'
        rule_names(8)%chars = 'hspace_complex_par'
        rule_names(9)%chars = 'hspace_deallocate_par'
        rule_names(10)%chars = 'hspace_dimension_par'
        rule_names(11)%chars = 'hspace_do_while'
        rule_names(12)%chars = 'hspace_dowhile_par'
        rule_names(13)%chars = 'hspace_else_if'
        rule_names(14)%chars = 'hspace_elseif_par'
        rule_names(15)%chars = 'hspace_end_associate'
        rule_names(16)%chars = 'hspace_end_block'
        rule_names(17)%chars = 'hspace_end_do'
        rule_names(18)%chars = 'hspace_end_forall'
        rule_names(19)%chars = 'hspace_end_function'
        rule_names(20)%chars = 'hspace_end_if'
        rule_names(21)%chars = 'hspace_end_interface'
        rule_names(22)%chars = 'hspace_end_module'
        rule_names(23)%chars = 'hspace_end_program'
        rule_names(24)%chars = 'hspace_end_select'
        rule_names(25)%chars = 'hspace_end_submodule'
        rule_names(26)%chars = 'hspace_end_subroutine'
        rule_names(27)%chars = 'hspace_end_where'
        rule_names(28)%chars = 'hspace_forall_par'
        rule_names(29)%chars = 'hspace_if_par'
        rule_names(30)%chars = 'hspace_integer_par'
        rule_names(31)%chars = 'hspace_intent_par'
        rule_names(32)%chars = 'hspace_logical_par'
        rule_names(33)%chars = 'hspace_open_par'
        rule_names(34)%chars = 'hspace_par_then'
        rule_names(35)%chars = 'hspace_print_star'
        rule_names(36)%chars = 'hspace_real_par'
        rule_names(37)%chars = 'hspace_result_par'
        rule_names(38)%chars = 'hspace_rewind_par'
        rule_names(39)%chars = 'hspace_separator'
        rule_names(40)%chars = 'hspace_type_par'
        rule_names(41)%chars = 'hspace_typeis_par'
        rule_names(42)%chars = 'hspace_where_par'
        rule_names(43)%chars = 'hspace_write_par'
        rule_names(44)%chars = 'indent_associate'
        rule_names(45)%chars = 'indent_block'
        rule_names(46)%chars = 'indent_case_clause'
        rule_names(47)%chars = 'indent_classis_clause'
        rule_names(48)%chars = 'indent_continued_line'
        rule_names(49)%chars = 'indent_default_class_clause'
        rule_names(50)%chars = 'indent_do'
        rule_names(51)%chars = 'indent_forall'
        rule_names(52)%chars = 'indent_function'
        rule_names(53)%chars = 'indent_if'
        rule_names(54)%chars = 'indent_interface'
        rule_names(55)%chars = 'indent_module'
        rule_names(56)%chars = 'indent_prog'
        rule_names(57)%chars = 'indent_select'
        rule_names(58)%chars = 'indent_submodule'
        rule_names(59)%chars = 'indent_subroutine'
        rule_names(60)%chars = 'indent_type'
        rule_names(61)%chars = 'indent_typeis_clause'
        rule_names(62)%chars = 'indent_where'
        rule_names(63)%chars = 'unindent_case'
        rule_names(64)%chars = 'unindent_contains'
        rule_names(65)%chars = 'unindent_else'
        rule_names(66)%chars = 'unindent_elsewhere'

        ! Count number of rules that need to be run
        num_rules_to_run = 0
        do i=1,style%num_rules
            if (style%options%get_val(rule_names(i)%chars)) then
                num_rules_to_run = num_rules_to_run + 1
            end if
        end do

        ! Construct list of rules that need to be run
        allocate (rules_to_run(num_rules_to_run))
        j = 1
        do i=1,style%num_rules
            if (style%options%get_val(rule_names(i)%chars)) then
                rules_to_run(j)%routine => rule_list(i)%routine
                j = j + 1
            end if
        end do
    end function


    subroutine generic_hspace_between(source, keyword1, keyword2, expression, num_spaces)
        !! Generic subroutine for setting the number of spaces between two keywords
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.
        character (len=*), intent (in) :: keyword1 !! Keyword before spacing.
        character (len=*), intent (in) :: keyword2 !! Keyword after spacing.
        character (len=*), intent (in) :: expression !! Regular expession.
        integer, intent (in) :: num_spaces !! Number of spaces to separate.
        integer :: i
        type (Regex) :: keyword_regex
        type (Match) :: match_keyword

        call keyword_regex%compile(expression, PCRE_CASELESS)

        do i=1,source%nlines
            match_keyword = keyword_regex%match(source%lines(i)%chars)
            if (match_keyword%matches) then
                source%lines(i)%chars = keyword_regex%substitute(source%lines(i)%chars, &
                        keyword1 // repeat(' ', num_spaces) // keyword2)
            end if
        end do
    end subroutine


    subroutine hspace_separator(source)
        !! Sets the number of spaces surrounding the :: separator.
        use regex_m, only: separator_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        integer :: i
        type (Match) :: match_separator
        type (Regex) :: separator_regex
        type (SourceFile), intent (inout) :: source !! Input source file.

        call separator_regex%compile(separator_exp, PCRE_CASELESS)

        do i=1,source%nlines
            match_separator = separator_regex%match(source%lines(i)%chars)
            if (match_separator%matches) then
                source%lines(i)%chars = separator_regex%substitute(source%lines(i)%chars, &
                        repeat(' ', style%num_hspace_separator) // '::' // repeat(' ', style%num_hspace_separator))
            end if
        end do
    end subroutine hspace_separator


    subroutine hspace_class_par(source)
        !! Sets the number of spaces between the class keyword and the following opening
        !! parenthesis.
        use regex_m, only: class_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'class', '(', class_par_exp, style%num_hspace_class_par)
    end subroutine hspace_class_par


    subroutine hspace_dimension_par(source)
        !! Sets the number of spaces between the dimension keyword and the following opening
        !! parenthesis.
        use regex_m, only: dimension_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'dimension', '(', dimension_par_exp, style%num_hspace_dimension_par)
    end subroutine hspace_dimension_par


    subroutine hspace_do_while(source)
        !! Sets the number of spaces between the do keyword and the while keyword.
        use regex_m, only: do_while_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'do', 'while', do_while_exp, style%num_hspace_do_while)
    end subroutine hspace_do_while


    subroutine hspace_par_then(source)
        !! Sets the number of spaces between a ) and the then keyword.
        use regex_m, only: par_then_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, ')', 'then', par_then_exp, style%num_hspace_par_then)
    end subroutine hspace_par_then


    subroutine hspace_print_star(source)
        !! Sets the number of spaces between a print statement and a format specifier.
        use regex_m, only: print_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        integer :: i
        type (Match) :: match_print
        type (Regex) :: print_regex
        type (SourceFile), intent (inout) :: source !! Input source file.

        call print_regex%compile(print_exp, PCRE_CASELESS)

        do i=1,source%nlines
            match_print = print_regex%match(source%lines(i)%chars)
            if (match_print%matches) then
                source%lines(i)%chars = print_regex%substitute(source%lines(i)%chars, &
                        'print' // repeat(' ', style%num_hspace_par_then))
            end if
        end do
    end subroutine hspace_print_star


    subroutine hspace_else_if(source)
        !! Sets the number of spaces between the do keyword and the while keyword.
        use regex_m, only: else_if_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'else', 'if', else_if_exp, style%num_hspace_else_if)
    end subroutine hspace_else_if


    subroutine hspace_end_do(source)
        use regex_m, only: end_do_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'end', 'do', end_do_exp, style%num_hspace_end_do)
    end subroutine


    subroutine hspace_end_program(source)
        use regex_m, only: end_program_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'end', 'program', end_program_exp, style%num_hspace_end_program)
    end subroutine


    subroutine hspace_end_if(source)
        use regex_m, only: end_if_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'end', 'if', end_if_exp, style%num_hspace_end_if)
    end subroutine


    subroutine hspace_end_module(source)
        use regex_m, only: end_module_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'end', 'module', end_module_exp, style%num_hspace_end_module)
    end subroutine


    subroutine hspace_end_function(source)
        use regex_m, only: end_function_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'end', 'function', end_function_exp, style%num_hspace_end_function)
    end subroutine


    subroutine hspace_end_subroutine(source)
        use regex_m, only: end_subroutine_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'end', 'subroutine', end_subroutine_exp, style%num_hspace_end_subroutine)
    end subroutine


    subroutine hspace_end_select(source)
        use regex_m, only: simple_end_select_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'end', 'select', simple_end_select_exp, style%num_hspace_end_select)
    end subroutine


    subroutine hspace_end_interface(source)
        use regex_m, only: simple_end_interface_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'end', 'interface', simple_end_interface_exp, style%num_hspace_end_interface)
    end subroutine


    subroutine hspace_end_where(source)
        use regex_m, only: end_where_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'end', 'where', end_where_exp, style%num_hspace_end_where)
    end subroutine


    subroutine hspace_end_submodule(source)
        use regex_m, only: end_submodule_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'end', 'submodule', end_submodule_exp, style%num_hspace_end_submodule)
    end subroutine


    subroutine hspace_end_block(source)
        use regex_m, only: simple_end_block_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'end', 'block', simple_end_block_exp, style%num_hspace_end_block)
    end subroutine


    subroutine hspace_end_associate(source)
        use regex_m, only: simple_end_associate_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'end', 'associate', simple_end_associate_exp, style%num_hspace_end_associate)
    end subroutine


    subroutine hspace_end_forall(source)
        use regex_m, only: simple_end_forall_exp
        use style_spec_m, only: style
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'end', 'forall', simple_end_forall_exp, style%num_hspace_end_forall)
    end subroutine


    subroutine hspace_elseif_par(source)
        !! Sets the number of spaces between the else if keyword and the following opening
        !! parenthesis.
        use regex_m, only: elseif_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'else if', '(',elseif_par_exp, style%num_hspace_elseif_par)
    end subroutine hspace_elseif_par


    subroutine hspace_dowhile_par(source)
        !! Sets the number of spaces between the do while keyword and the following opening
        !! parenthesis.
        use regex_m, only: dowhile_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'do while', '(', dowhile_par_exp, style%num_hspace_dowhile_par)
    end subroutine hspace_dowhile_par


    subroutine hspace_result_par(source)
        !! Sets the number of spaces between the result keyword and the following opening
        !! parenthesis.
        use regex_m, only: result_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'result', '(', result_par_exp, style%num_hspace_result_par)
    end subroutine hspace_result_par


    subroutine hspace_allocate_par(source)
        !! Sets the number of spaces between the allocate keyword and the following opening
        !! parenthesis.
        use regex_m, only: allocate_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'allocate', '(', allocate_par_exp, style%num_hspace_allocate_par)
    end subroutine hspace_allocate_par


    subroutine hspace_deallocate_par(source)
        !! Sets the number of spaces between the deallocate keyword and the following opening
        !! parenthesis.
        use regex_m, only: deallocate_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'deallocate', '(', deallocate_par_exp, style%num_hspace_deallocate_par)
    end subroutine hspace_deallocate_par


    subroutine hspace_associate_par(source)
        !! Sets the number of spaces between the associate keyword and the following opening
        !! parenthesis.
        use regex_m, only: associate_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'associate', '(', associate_par_exp, style%num_hspace_associate_par)
    end subroutine hspace_associate_par


    subroutine hspace_case_par(source)
        !! Sets the number of spaces between the case keyword and the following opening
        !! parenthesis.
        use regex_m, only: case_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'case', '(', case_par_exp, style%num_hspace_case_par)
    end subroutine hspace_case_par


    subroutine hspace_complex_par(source)
        !! Sets the number of spaces between the complex keyword and the following opening
        !! parenthesis.
        use regex_m, only: complex_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'complex', '(', complex_par_exp, style%num_hspace_complex_par)
    end subroutine hspace_complex_par


    subroutine hspace_character_par(source)
        !! Sets the number of spaces between the character keyword and the following opening
        !! parenthesis.
        use regex_m, only: character_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'character', '(', character_par_exp, style%num_hspace_character_par)
    end subroutine hspace_character_par


    subroutine hspace_close_par(source)
        !! Sets the number of spaces between the close keyword and the following opening
        !! parenthesis.
        use regex_m, only: close_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'close', '(', close_par_exp, style%num_hspace_close_par)
    end subroutine hspace_close_par


    subroutine hspace_forall_par(source)
        !! Sets the number of spaces between the forall keyword and the following opening
        !! parenthesis.
        use regex_m, only: forall_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'forall', '(', forall_par_exp, style%num_hspace_forall_par)
    end subroutine hspace_forall_par


    subroutine hspace_if_par(source)
        !! Sets the number of spaces between the if keyword and the following opening
        !! parenthesis.
        use regex_m, only: if_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'if', '(', if_par_exp, style%num_hspace_if_par)
    end subroutine hspace_if_par


    subroutine hspace_integer_par(source)
        !! Sets the number of spaces between the integer keyword and the following opening
        !! parenthesis.
        use regex_m, only: integer_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'integer', '(', integer_par_exp, style%num_hspace_integer_par)
    end subroutine hspace_integer_par


    subroutine hspace_intent_par(source)
        !! Sets the number of spaces between the intent keyword and the following opening
        !! parenthesis.
        use regex_m, only: intent_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'intent', '(', intent_par_exp, style%num_hspace_intent_par)
    end subroutine hspace_intent_par


    subroutine hspace_classis_par(source)
        !! Sets the number of spaces between the classis keyword and the following opening
        !! parenthesis.
        use regex_m, only: classis_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'classis', '(', classis_par_exp, style%num_hspace_classis_par)
    end subroutine hspace_classis_par


    subroutine hspace_typeis_par(source)
        !! Sets the number of spaces between the typeis keyword and the following opening
        !! parenthesis.
        use regex_m, only: typeis_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'type is', '(', typeis_par_exp, style%num_hspace_typeis_par)
    end subroutine hspace_typeis_par


    subroutine hspace_logical_par(source)
        !! Sets the number of spaces between the logical keyword and the following opening
        !! parenthesis.
        use regex_m, only: logical_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'logical', '(', logical_par_exp, style%num_hspace_logical_par)
    end subroutine hspace_logical_par


    subroutine hspace_open_par(source)
        !! Sets the number of spaces between the open keyword and the following opening
        !! parenthesis.
        use regex_m, only: open_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'open', '(', open_par_exp, style%num_hspace_open_par)
    end subroutine hspace_open_par


    subroutine hspace_real_par(source)
        !! Sets the number of spaces between the real keyword and the following opening
        !! parenthesis.
        use regex_m, only: real_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'real', '(', real_par_exp, style%num_hspace_real_par)
    end subroutine hspace_real_par


    subroutine hspace_rewind_par(source)
        !! Sets the number of spaces between the rewind keyword and the following opening
        !! parenthesis.
        use regex_m, only: rewind_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'rewind', '(', rewind_par_exp, style%num_hspace_rewind_par)
    end subroutine hspace_rewind_par


    subroutine hspace_type_par(source)
        !! Sets the number of spaces between the type keyword and the following opening
        !! parenthesis.
        use regex_m, only: type_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'type', '(', type_par_exp, style%num_hspace_type_par)
    end subroutine hspace_type_par


    subroutine hspace_where_par(source)
        !! Sets the number of spaces between the where keyword and the following opening
        !! parenthesis.
        use regex_m, only: where_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'where', '(', where_par_exp, style%num_hspace_where_par)
    end subroutine hspace_where_par


    subroutine hspace_write_par(source)
        !! Sets the number of spaces between the write keyword and the following opening
        !! parenthesis.
        use regex_m, only: write_par_exp
        use style_spec_m, only: style
        type (SourceFile), intent (inout) :: source !! Input source file.

        call generic_hspace_between(source, 'write', '(', write_par_exp, style%num_hspace_write_par)
    end subroutine hspace_write_par


    function indent_levels(source, start_exp, conclude_exp)
        !! This rule returns the indentation level for a code block between the given start and end
        !! regular expressions.
        use pcre_constants, only: PCRE_CASELESS
        use regex_m, only: end_exp, all_indentables_exp, end_all_indentables_exp
        type (SourceFile), intent (in) :: source !! Input source file.
        character (len=*), intent (in) :: start_exp, conclude_exp !! Expression to indent between.
        type (Regex) :: start_regex, &
                conclude_regex, &
                conclude_end_regex, &
                all_indentables_regex, &
                end_all_indentables_regex
        integer :: indent_levels(source%nlines), indent(source%nlines) ! for gdb debug purposes
        type (Match) :: match_start, &
                match_conclude, &
                match_conclude_end, &
                match_all_indentables, &
                match_end_all_indentables
        integer :: i
        integer :: current_lvl, potential_lvl
        character (len=300) :: preview

        indent = 0

        call start_regex%compile(start_exp, PCRE_CASELESS)
        call conclude_regex%compile(conclude_exp, PCRE_CASELESS)
        call conclude_end_regex%compile(end_exp, PCRE_CASELESS)
        call all_indentables_regex%compile(all_indentables_exp, PCRE_CASELESS)
        call end_all_indentables_regex%compile(end_all_indentables_exp, PCRE_CASELESS)

        potential_lvl = 0
        current_lvl = 0
        do i=1,source%nlines
            preview = source%lines(i)%chars ! for gdb debug purposes
            ! Check for start statement
            match_start = start_regex%match(source%lines(i)%chars)
            if (match_start%matches) then
                indent(i) = current_lvl
                current_lvl = current_lvl + 1
                potential_lvl = potential_lvl + 1
                cycle
            end if
            ! Check for end block statement
            match_conclude = conclude_regex%match(source%lines(i)%chars)
            if (match_conclude%matches) then
                current_lvl = current_lvl - 1
                potential_lvl = potential_lvl - 1
                indent(i) = current_lvl
                cycle
            end if
            ! Check for other start block statments
            match_all_indentables = all_indentables_regex%match(source%lines(i)%chars)
            if (match_all_indentables%matches.and.current_lvl<=potential_lvl &
                        .and.current_lvl>0) then
                potential_lvl = potential_lvl + 1
                indent(i) = current_lvl
                cycle
            end if
           ! Check for end all other indentables to enable end statement
            match_end_all_indentables = end_all_indentables_regex%match(source%lines(i)%chars)
            if (match_end_all_indentables%matches.and.(potential_lvl>current_lvl)) then
                potential_lvl = potential_lvl - 1
                indent(i) = current_lvl
                cycle
            end if
            ! Check for end statement
            match_conclude_end = conclude_end_regex%match(source%lines(i)%chars)
            if (match_conclude_end%matches.and.(potential_lvl==current_lvl) &
                        .and.current_lvl>0) then
                current_lvl = current_lvl - 1
                potential_lvl = potential_lvl - 1
                indent(i) = current_lvl
                cycle
            else if (match_conclude_end%matches.and.(potential_lvl>current_lvl)) then
                potential_lvl = potential_lvl - 1
                indent(i) = current_lvl
                cycle
            end if
            indent(i) = current_lvl
        end do
        indent_levels = indent
    end function indent_levels

    subroutine indent_associate(source)
        !! Increase the indentation of associate blocks.
        use regex_m, only: start_associate_exp, end_associate_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_levels(source, start_associate_exp, &
                end_associate_exp)
    end subroutine

    subroutine indent_block(source)
        !! Increase the indentation level of the block statements of a source file.
        use regex_m, only: start_block_exp, end_block_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_levels( &
                source, &
                start_block_exp, &
                end_block_exp)
    end subroutine

    subroutine indent_case_clause(source)
        !! Increase the indentation of case clauses.
        use regex_m, only: case_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_clause(source, case_exp)
    end subroutine

    subroutine indent_classis_clause(source)
        !! Increase the indentation of case clauses.
        use regex_m, only: typeis_exp, classis_exp, default_class_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_clause( &
                source, classis_exp, typeis_exp, default_class_exp)
    end subroutine

    function indent_clause(source, clause_exp, ignore_clause_exp, ignore_clause2_exp) result (indent)
        use regex_m, only: end_select_exp
        use pcre_constants, only: PCRE_CASELESS
        !! This rule increases the indent level for case, typeis, classis and default clauses
        type (SourceFile), intent (in) :: source !! Source file
        character (len=*), intent (in) :: clause_exp !! Clause to indent
        character (len=*), intent (in), optional :: ignore_clause_exp !! Clause to ignore
        character (len=*), intent (in), optional :: ignore_clause2_exp !! Clause to ignore
        integer :: indent(source%nlines)
        type (Regex) :: clause_regex, end_select_regex, ignore_clause_regex, ignore_clause2_regex
        type (Match) :: match_clause, match_end_select, match_ignore_clause, match_ignore_clause2
        integer :: i
        logical :: in_clause

        call clause_regex%compile(clause_exp, PCRE_CASELESS)
        call end_select_regex%compile(end_select_exp, PCRE_CASELESS)
        if (present(ignore_clause_exp)) then
            call ignore_clause_regex%compile(ignore_clause_exp, PCRE_CASELESS)
        end if
        if (present(ignore_clause2_exp)) then
            call ignore_clause2_regex%compile(ignore_clause2_exp, PCRE_CASELESS)
        end if

        indent = 0
        in_clause = .false.
        do i=1,source%nlines
            match_clause = clause_regex%match(source%lines(i)%chars)
            if (match_clause%matches) then
                indent(i) = 0
                in_clause = .true.
                cycle
            end if
            if (present(ignore_clause_exp)) then
                match_ignore_clause = ignore_clause_regex%match(source%lines(i)%chars)
                if (match_ignore_clause%matches) then
                    in_clause = .false.
                    indent(i) = 0
                    cycle
                end if
            end if
            if (present(ignore_clause2_exp)) then
                match_ignore_clause2 = ignore_clause2_regex%match(source%lines(i)%chars)
                if (match_ignore_clause2%matches) then
                    in_clause = .false.
                    indent(i) = 0
                    cycle
                end if
            end if
            match_end_select = end_select_regex%match(source%lines(i)%chars)
            if (match_end_select%matches) then
                indent(i) = 0
                in_clause = .false.
                cycle
            end if
            if (in_clause) then
                indent(i) = 1
            end if
        end do
    end function

    subroutine indent_continued_line(source)
        !! This rule increases the indentation for a continued line.
        use pcre_constants, only: PCRE_CASELESS
        use style_spec_m, only: style
        use regex_m, only: line_continue_exp
        type (SourceFile), intent (inout) :: source
        character (len=500) :: previous_line
        type (Regex) :: statement_regex
        type (Match) :: match_statement
        integer :: i

        call statement_regex%compile(line_continue_exp, PCRE_CASELESS)

        do i=2,source%nlines
            previous_line = source%lines(i-1)%chars
            previous_line = mask_comment(previous_line, remove_quotes=.true.)
            match_statement = statement_regex%match(previous_line)
            if (match_statement%matches) then
                source%indent_lvls(i) = source%indent_lvls(i) + style%num_of_continue_indents
            end if
        end do
    end subroutine

    subroutine indent_default_class_clause(source)
        !! Increase the indentation of case clauses.
        use regex_m, only: typeis_exp, classis_exp, default_class_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_clause( &
                source, default_class_exp, typeis_exp, classis_exp)
    end subroutine

    subroutine indent_do(source)
        !! Increase the indentation level of the do blocks of a source file.
        use regex_m, only: start_do_exp, end_do_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_levels(source, start_do_exp, end_do_exp)
    end subroutine

    subroutine indent_forall(source)
        !! Increase the indentation level of the forall statements of a source file.
        use regex_m, only: start_forall_exp, end_forall_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_levels( &
                source, &
                start_forall_exp, &
                end_forall_exp)
    end subroutine

    subroutine indent_function(source)
        !! Increase the indentation level of the function blocks of a source file.
        use regex_m, only: start_function_exp, end_function_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_levels( &
                source, &
                start_function_exp, &
                end_function_exp)
    end subroutine

    subroutine indent_if(source)
        !! Increase the indentation level of the if blocks of a source file.
        use regex_m, only: start_if_exp, end_if_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_levels(source, start_if_exp, end_if_exp)
        source%indent_lvls = source%indent_lvls + indent_if_continued(source)
    end subroutine

    function indent_if_continued(source)
        use regex_m, only: line_continue_exp
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (inout) :: source
        integer :: indent_if_continued(source%nlines)
        type (Regex) :: if_cont_regex, then_regex, continue_regex
        type (Match) :: match_if_cont, match_then, match_continue
        integer :: current_lvl = 0
        integer :: i = 1
        logical :: is_continued = .false.

        indent_if_continued = 0
        call if_cont_regex%compile("^[ \t]*if.*&", PCRE_CASELESS)
        call then_regex%compile(".* then(?![\w\d])", PCRE_CASELESS)
        call continue_regex%compile(line_continue_exp, PCRE_CASELESS)
        do while (i<=source%nlines)
            indent_if_continued(i) = current_lvl
            match_if_cont = if_cont_regex%match(source%lines(i)%chars)
            if (match_if_cont%matches) then
                is_continued = .true.
                do while (is_continued)
                    i = i + 1
                    indent_if_continued(i) = current_lvl
                    match_then = then_regex%match(source%lines(i)%chars)
                    match_continue = continue_regex%match(source%lines(i)%chars)
                    if (match_then%matches) then
                        current_lvl = current_lvl + 1
                        is_continued = .false.
                    else if (.not.match_continue%matches) then
                        is_continued = .false.
                    end if
                end do
            end if
            i = i + 1
        end do
    end function indent_if_continued

    subroutine indent_interface(source)
        !! Increase the indentation level of the interface blocks of a source file.
        use regex_m, only: start_interface_exp, end_interface_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_levels( &
                source, &
                start_interface_exp, &
                end_interface_exp)
    end subroutine

    subroutine indent_module(source)
        !! Increase the indentation level of the module blocks of a source file.
        use regex_m, only: start_module_exp, end_module_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls +indent_levels( &
                source, &
                start_module_exp, &
                end_module_exp)
    end subroutine indent_module

    subroutine indent_prog(source)
        !! Increase the indentation level of the program block of a source file.
        use regex_m, only: start_program_exp, end_program_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_levels( &
                source, &
                start_program_exp, &
                end_program_exp)
    end subroutine

    subroutine indent_select(source)
        !! Increase the indentation level of the select statements of a source file.
        use regex_m, only: start_select_exp, end_select_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_levels( &
                source, &
                start_select_exp, &
                end_select_exp)
    end subroutine

    subroutine indent_submodule(source)
        !! Increase the indentation level of the submodule blocks of a source file.
        use regex_m, only: start_submodule_exp, end_submodule_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls +indent_levels( &
                source, &
                start_submodule_exp, &
                end_submodule_exp)
    end subroutine indent_submodule

    subroutine indent_subroutine(source)
        !! Increase the indentation level of the subroutine blocks of a source file.
        use regex_m, only: start_subroutine_exp, end_subroutine_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_levels( &
                source, &
                start_subroutine_exp, &
                end_subroutine_exp)
    end subroutine

    subroutine indent_type(source)
        !! Increase the indentation level of the type definitiona of a source file.
        use regex_m, only: start_type_exp, end_type_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_levels( &
                source, &
                start_type_exp, &
                end_type_exp)
    end subroutine

    subroutine indent_typeis_clause(source)
        !! Increase the indentation of case clauses.
        use regex_m, only: typeis_exp, classis_exp, default_class_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_clause( &
                source, typeis_exp, classis_exp, default_class_exp)
    end subroutine

    subroutine indent_where(source)
        !! Increase the indentation level of the where blocks of a source file.
        use regex_m, only: start_where_exp, end_where_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + indent_levels( &
                source, &
                start_where_exp, &
                end_where_exp)
    end subroutine

    function mask_quotes(string) result (out_string)
        !! Mask with whitesppace all characters in string literals enclosed in single and double
        !! quote matks.
        character (len=*), intent (in) :: string
        character (len=len(string)) :: out_string
        character (len=1) :: c
        logical :: in_quote
        integer :: i

        out_string = string
        in_quote = .false.
        do i = 1,len(string)
            c = out_string(i:i)
            if (in_quote.and.(c=='"'.or.c=="'")) in_quote = .false.
            if (in_quote) out_string(i:i) = ' '
            if (.not.in_quote.and.(c=='"'.or.c=="'")) in_quote = .true.
        end do
    end function mask_quotes

    function mask_comment(string, remove_quotes) result (out_string)
        !! Mask commented text with whitespace. Expects that there are no ! characters enclosed in
        !! quotes by default. remove_quotes=.true. will run mas_quotes on the string first.
        logical, optional :: remove_quotes
        character (len=*), intent (in) :: string
        character (len=len(string)) :: out_string
        character (len=1) :: c
        logical :: in_comment
        integer :: i

        out_string = string
        if (.not.present(remove_quotes)) remove_quotes = .false.
        if (remove_quotes) out_string = mask_quotes(out_string)

        in_comment = .false.
        do i = 1,len(string)
            c = out_string(i:i)
            if (in_comment) then
                out_string(i:i) = ' '
            else if (c=='!') then
                in_comment = .true.
            end if
        end do
    end function mask_comment

    subroutine unindent_case(source)
        !! Decrease the indentation level of case statements.
        use regex_m, only: case_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + unindent_statement(source, case_exp)
        where (source%indent_lvls<0)
            source%indent_lvls = 0
        end where
    end subroutine

    subroutine unindent_contains(source)
        !! Decrease the indentation level of contains statements.
        use regex_m, only: contains_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + unindent_statement(source, contains_exp)
        where (source%indent_lvls<0)
            source%indent_lvls = 0
        end where
    end subroutine

    subroutine unindent_else(source)
        !! Decrease the indentation level of else and else if statements.
        use regex_m, only: else_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + unindent_statement(source, else_exp)
        where (source%indent_lvls<0)
            source%indent_lvls = 0
        end where
    end subroutine

    subroutine unindent_elsewhere(source)
        !! Decrease the indentation level of case statements.
        use regex_m, only: elsewhere_exp
        type (SourceFile), intent (inout) :: source

        source%indent_lvls = source%indent_lvls + unindent_statement(source, elsewhere_exp)
        where (source%indent_lvls<0)
            source%indent_lvls = 0
        end where
    end subroutine

    function unindent_statement(source, statement_exp) result (indent)
        !! This rule decreases the indentation for a specific Fortran statement.
        use pcre_constants, only: PCRE_CASELESS
        type (SourceFile), intent (in) :: source
        character (len=*), intent (in) :: statement_exp
        integer :: indent(source%nlines)
        type (Regex) :: statement_regex
        type (Match) :: match_statement
        integer :: i

        call statement_regex%compile(statement_exp, PCRE_CASELESS)

        indent = 0
        do i=1,source%nlines
            match_statement = statement_regex%match(source%lines(i)%chars)
            if (match_statement%matches) then
                indent(i) = -1
            end if
        end do
    end function

end module rules_m

! Copyright (C) 2021  Tammas Loughran
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
