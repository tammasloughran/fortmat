module cli_m
    use M_CLI2, only: set_args, get_args, filenames=>unnamed
    implicit none

    logical :: print_to_stdout !! Flag for whether to print to standard output
    logical :: output_spec !! Flag for whether to output the default style specification namelist
    character (len=:), allocatable :: style_spec_file

contains

    subroutine parse_cli()
        !! Parse the command line arguments.
        !! Usage:
        !!     fortmat [options] <file>...
        !!
        !! A fortran source code formatter.
        !!
        !! Options:
        !!  -p               Print formatted text to stdout instead of overwriting input
        !!                       files.
        !!  -s --style-spec  Namelist file containing the style specification.
        !!  --output-spec    Generate the default style specification namelist.
        !!  --help           Display this help.
        !!  --version        Display version.
        character (len=:), allocatable :: help_text(:), version_text(:)
        character (len=*), parameter :: command_proto = '&
            &-p F &
            &--style-spec:s " " &
            &--output-spec F &
            &'

        help_text = [character (len=80) :: &
            &'Usage:                                                                         ',&
            &'    fortmat [options] <file>...                                                ',&
            &'                                                                               ',&
            &'A fortran source code formatter.                                               ',&
            &'                                                                               ',&
            &'Options:                                                                       ',&
            &' -p               Print formatted text to stdout instead of overwriting input  ',&
            &'                      files.                                                   ',&
            &' -s --style-spec  Namelist file containing the style specification.            ',&
            &' --output-spec    Generate the default style specification namelist.           ',&
            &' --help           Display this help.                                           ',&
            &' --version        Display version.                                             ',&
            &'']
        version_text = [character (len=80) :: &
            &'Version:   0.1.0, alpha                                                        ',&
            &'Program:   fortmat                                                             ',&
            &'OS:        Linux                                                               ']

        ! Get the arguments
        call set_args(command_proto, help_text, version_text)
        call get_args('p', print_to_stdout)
        call get_args('style-spec', style_spec_file)
        call get_args('output-spec', output_spec)
    end subroutine parse_cli

end module cli_m

! Copyright (C) 2021  Tammas Loughran
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
