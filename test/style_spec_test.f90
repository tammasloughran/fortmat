module style_spec_test
    use style_spec_m, only: style_spec_t, construct_style_spec
    use vegetables

contains

    function test_style_spec() result (tests)
        type (test_item_t) :: tests

        tests = describe("construct_style_spec", [ &
                it("style contains indent_width", check_style_contains_indent_width), &
                it("style contins options", check_style_contains_options) ])
    end function test_style_spec

    function check_style_contains_indent_width() result (result_)
        use style_spec_m, only: style
        type (result_t) :: result_

        call construct_style_spec()
        result_ = assert_that(style%indent_width==4)
    end function check_style_contains_indent_width

    function check_style_contains_options() result (result_)
        use style_spec_m, only: style
        type (result_t) :: result_

        call construct_style_spec()
        result_ = assert_that(style%options%get_val('unindent_contains'))
    end function check_style_contains_options

end module style_spec_test
