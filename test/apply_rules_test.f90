module apply_rules_test
    use sourcefile_m, only: SourceFile
    use vegetables
    implicit none
    private

    public :: test_apply_rules, test_apply_indents

contains

    function test_apply_rules() result (tests)
        type (test_item_t) :: tests

        tests = describe("apply_rules_to_file", [ &
                it("applies a rule", check_apply_rules_to_file_works)])
    end function test_apply_rules

    function test_apply_indents() result (tests)
        type (test_item_t) :: tests

        tests = describe("apply_indents", [ &
                it("indents a file", check_apply_indents_works)])
    end function

    function check_apply_rules_to_file_works() result (result_)
        use apply_rules_m, only: apply_rules_to_file
        use rules_m, only: rule_list_t, indent_prog
        type (result_t) :: result_
        type (SourceFile) :: source
        type (rule_list_t) :: rules_to_run(1)
        integer :: testfile_unit, expected(5)

        ! Setup
        open (newunit=testfile_unit, file='testfile_apply_rules', status='new', action='write')
        write (unit=testfile_unit, fmt='(A)') "program foo"
        write (unit=testfile_unit, fmt='(A)') "implicit none"
        write (unit=testfile_unit, fmt='(A)') "integer :: i"
        write (unit=testfile_unit, fmt='(A)') "i = 1"
        write (unit=testfile_unit, fmt='(A)') "end program"
        close (unit=testfile_unit)
        source = SourceFile('testfile_apply_rules')
        rules_to_run(1)%routine => indent_prog
        expected = [0,1,1,1,0]

        ! Run
        call apply_rules_to_file(rules_to_run, source)

        ! Teardown
        close (unit=source%iounit, status='delete')

        ! Test
        result_ = assert_integer_array_equals(source%indent_lvls, expected)
    contains
        function assert_integer_array_equals(actual, expected2) result (result_)
            integer, intent (in) :: actual(:), expected2(:)
            type (result_t) :: result_
            if (all(actual==expected2)) then
                result_ = succeed("")
            else
                result_ = fail("arrays do not match")
                print *, actual
                print *, expected2
            end if
        end function
    end function check_apply_rules_to_file_works

    function check_apply_indents_works() result (result_)
        use apply_rules_m, only: apply_indents
        use style_spec_m, only: construct_style_spec
        use cli_m
        type (SourceFile) :: source
        type (result_t) :: result_
        integer :: testfile_unit
        character (len=17) :: output

        ! Setup
        call construct_style_spec()
        open (newunit=testfile_unit, file='testfile_apply_rules', status='new', action='write')
        write (unit=testfile_unit, fmt='(A)') "program foo"
        write (unit=testfile_unit, fmt='(A)') "implicit none"
        write (unit=testfile_unit, fmt='(A)') "integer :: i"
        write (unit=testfile_unit, fmt='(A)') "i = 1"
        write (unit=testfile_unit, fmt='(A)') "end program"
        close (unit=testfile_unit)
        source = SourceFile('testfile_apply_rules')
        source%indent_lvls(2) = 1

        ! Run
        call apply_indents(source)
        output = source%lines(2)%chars

        ! Teardown
        close (unit=source%iounit, status='delete')

        ! Test
        result_ = assert_character_match(output, '    implicit none')
    contains
        function assert_character_match(actual, expected2) result (result_)
            character (len=*), intent (in) :: actual, expected2
            type (result_t) :: result_
            if (actual==expected2) then
                result_ = succeed("")
            else
                result_ = fail("indented character strings do not match")
                print *, actual
                print *, expected2
            end if
        end function
    end function check_apply_indents_works

end module apply_rules_test
