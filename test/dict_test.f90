module dict_test
    use dict_m, only: LogicalDict
    use vegetables
    implicit none

contains

    function test_logicaldict() result (tests)
        type (test_item_t) :: tests

        tests = describe("LogicalDict", [ &
                it("constructor creates length of dict", check_logicaldict_constructor_works), &
                it("constructor length and keys match", check_logicaldict_length_keys_match), &
                it("constructor length and vals match", check_logicaldict_length_vals_match), &
                it("constructor keys and vals match", check_logicaldict_keys_vals_match), &
                it("get_val returns a value", check_logicaldict_get_val), &
                it("set_key_val_pair creates a new entry", check_logicaldict_set_key_val)])
    end function test_logicaldict

    function check_logicaldict_constructor_works() result (result_)
        type (result_t) :: result_
        type (LogicalDict) :: dict

        dict = LogicalDict(2)
        result_ = assert_that(dict%length==2)
    end function check_logicaldict_constructor_works

    function check_logicaldict_length_keys_match() result (result_)
        type (result_t) :: result_
        type (LogicalDict) :: dict

        dict = LogicalDict(2)
        result_ = assert_that(dict%length==size(dict%keys))
    end function check_logicaldict_length_keys_match

    function check_logicaldict_length_vals_match() result (result_)
        type (result_t) :: result_
        type (LogicalDict) :: dict

        dict = LogicalDict(2)
        result_ = assert_that(dict%length==size(dict%vals))
    end function check_logicaldict_length_vals_match

    function check_logicaldict_keys_vals_match() result (result_)
        type (result_t) :: result_
        type (LogicalDict) :: dict

        dict = LogicalDict(2)
        result_ = assert_that(size(dict%keys)==size(dict%vals))
    end function check_logicaldict_keys_vals_match

    function check_logicaldict_get_val() result (result_)
        type (result_t) :: result_
        type (LogicalDict) :: dict

        dict = LogicalDict(2)
        dict%keys(1)%chars = 'akey'
        dict%vals(1) = .true.
        dict%filled = 1
        result_ = assert_that(dict%get_val('akey'))
    end function check_logicaldict_get_val

    function check_logicaldict_set_key_val() result (result_)
        type (result_t) :: result_
        type (LogicalDict) :: dict
        logical :: checks(3) = .false.

        dict = LogicalDict(2)
        call dict%set_key_val_pair('one', .true.)
        call dict%set_key_val_pair('two', .true.)
        if ((dict%keys(1)%chars=='one').and.(dict%vals(1))) checks(1) = .true.
        if ((dict%keys(2)%chars=='two').and.(dict%vals(2))) checks(2) = .true.
        if (dict%filled==2) checks(3) = .true.
        result_ = assert_that(all(checks))
    end function check_logicaldict_set_key_val

end module dict_test
