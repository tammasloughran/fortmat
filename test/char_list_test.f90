module char_list_test
    use sourcefile_m, only: char_list_t
    use vegetables
    implicit none
    private

    public :: test_char_list

contains

    function test_char_list() result (tests)
        type (test_item_t) :: tests

        tests = describe("char_list_t", [ &
            it("contains character attribute", check_char_list_t_chars)])
    end function test_char_list

    function check_char_list_t_chars() result (result_)
        type (result_t) :: result_
        type (char_list_t) :: char_list

        char_list%chars = 'characters'
        result_ = assert_that(is_char(char_list%chars))
    contains
        logical function is_char(thing)
            class (*), intent (in) :: thing
            select type (thing)
                type is (character (len=*))
                    is_char = .true.
                class default
                    is_char = .false.
            end select
        end function
    end function check_char_list_t_chars

end module char_list_test

