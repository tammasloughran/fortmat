module indent_rules_test
    use sourcefile_m, only: SourceFile
    use vegetables
    implicit none

contains

    function test_indent_rules() result (tests)
        type (test_item_t) :: tests

        tests = describe("indent rules", [ &
                it("indent_associate increases indent level", check_indent_associate), &
                it("indent_block increases indent level", check_indent_block), &
                it("indent_case_clause increases indent level", check_indent_case_clause), &
                it("indent_classis_clause increases indent level", check_indent_classis_clause), &
                it("indent_continued_line increases indent level", check_indent_continued_line), &
                it("indent_default_class_clause increases indent level", check_indent_default_class_clause), &
                it("indent_do increases indent level", check_indent_do), &
                it("indent_do increases indent level with labels", check_indent_labeled), &
                it("indent_forall increases indent level", check_indent_forall), &
                it("indent_function increases indent level", check_indent_function), &
                it("indent_if does not indent single statement if", check_single_if), &
                it("indent_if increases indent level", check_indent_if), &
                it("indent_interface increases abstract interface", check_indent_abstract_interface), &
                it("indent_interface increases indent level", check_indent_interface), &
                it("indent_levels detects end statement", check_indent_levels_with_end), &
                it("indent_levels identifies a block of code", check_indent_levels_identifies), &
                it("indent_levels identifies multiple blocks", check_indent_levels_multiple), &
                it("indent_module ignores module procedures", check_ignore_module_procedure), &
                it("indent_module increases indent level", check_indent_module), &
                it("indent_prog increases indent level", check_indent_prog), &
                it("indent_select increases indent level", check_indent_select), &
                it("indent_submodule increases indent level", check_indent_submodule), &
                it("indent_subroutine increases indent level", check_indent_subroutine), &
                it("indent_type increases indent level", check_indent_type), &
                it("indent_typeis_clause increases indent level", check_indent_typeis_clause), &
                it("indent_where increases indent level", check_indent_where), &
                it("unindent_case decreases indent level", check_unindent_case), &
                it("unindent_contians decreases indent level", check_unindent_contains), &
                it("unindent_else decreases indent level", check_unindent_else), &
                it("unindent_elsewhere decreases indent level", check_unindent_elsewhere), &
                it("unindent_statement unindents a line", check_unindent_statement) ])
    end function


    function test_a_rules_list() result (tests)
        type (test_item_t) :: tests

        tests = describe("rules_list_t", [ &
                it("constructor works", check_construct_rules_list_works), &
                it("can associate a routine pointer", check_rules_list_associate) ])
    end function


    function check_construct_rules_list_works() result (result_)
        use rules_m, only: construct_rules_list, rule_list_t
        type (result_t) :: result_
        type (rule_list_t), allocatable :: rules(:)

        rules = construct_rules_list()
        result_ = assert_that(size(rules)>0)
    end function


    function check_ignore_module_procedure() result (result_)
        use rules_m, only: indent_module
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 5
        allocate (source%lines(5))
        source%lines(1)%chars = 'module test'
        source%lines(2)%chars = 'interface'
        source%lines(3)%chars = 'module procedure'
        source%lines(4)%chars = 'end interface'
        source%lines(5)%chars = 'end module test'
        allocate (source%indent_lvls(5))
        source%indent_lvls = 0

        call indent_module(source)
        result_ = assert_that(source%indent_lvls(4)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_abstract_interface() result (result_)
        use rules_m, only: indent_interface
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'abstract interface'
        source%lines(2)%chars = 'function foo()'
        source%lines(3)%chars = 'end interface'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_interface(source)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_associate() result (result_)
        use rules_m, only: indent_associate
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'associate (a=>b)'
        source%lines(2)%chars = 'call thing()'
        source%lines(3)%chars = 'end associate'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_associate(source)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_block() result (result_)
        use rules_m, only: indent_block
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'block'
        source%lines(2)%chars = 'integer :: foo'
        source%lines(3)%chars = 'end block'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_block(source)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_case_clause() result (result_)
        use rules_m, only: indent_case_clause
        type (result_t) :: result_
        type (SourceFile) :: source
        integer :: expected(6)

        expected = [0,0,1,0,1,0]
        source%nlines = 6
        allocate (source%lines(6))
        source%lines(1)%chars = 'select case (foo)'
        source%lines(2)%chars = 'case (1)'
        source%lines(3)%chars = 'statement'
        source%lines(4)%chars = 'case (2)'
        source%lines(5)%chars = 'statement'
        source%lines(6)%chars = 'end select'
        allocate (source%indent_lvls(6))
        source%indent_lvls = 0

        call indent_case_clause(source)
        result_ = assert_that(all(source%indent_lvls==expected))
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_classis_clause() result (result_)
        use rules_m, only: indent_classis_clause
        type (result_t) :: result_
        type (SourceFile) :: source
        integer :: expected(6)

        expected = [0,0,0,0,1,0]
        source%nlines = 6
        allocate (source%lines(6))
        source%lines(1)%chars = 'select type (foo)'
        source%lines(2)%chars = 'type is (typea)'
        source%lines(3)%chars = 'statement'
        source%lines(4)%chars = 'class is (classa)'
        source%lines(5)%chars = 'statement'
        source%lines(6)%chars = 'end select'
        allocate (source%indent_lvls(6))
        source%indent_lvls = 0

        call indent_classis_clause(source)
        result_ = assert_that(all(source%indent_lvls==expected))
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_continued_line() result (result_)
        use rules_m, only: indent_continued_line
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'this will be continued &'
        source%lines(2)%chars = 'this is continued ! &'
        source%lines(3)%chars = 'this is not continued'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_continued_line(source)
        result_ = assert_that((source%indent_lvls(2)==2.and.(source%indent_lvls(3)==0)))
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_default_class_clause() result (result_)
        use rules_m, only: indent_default_class_clause
        type (result_t) :: result_
        type (SourceFile) :: source
        integer :: expected(6)

        expected = [0,0,0,0,1,0]
        source%nlines = 6
        allocate (source%lines(6))
        source%lines(1)%chars = 'select type (foo)'
        source%lines(2)%chars = 'type is (typea)'
        source%lines(3)%chars = 'statement'
        source%lines(4)%chars = 'default class'
        source%lines(5)%chars = 'statement'
        source%lines(6)%chars = 'end select'
        allocate (source%indent_lvls(6))
        source%indent_lvls = 0

        call indent_default_class_clause(source)
        result_ = assert_that(all(source%indent_lvls==expected))
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_do() result (result_)
        use rules_m, only: indent_do
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'do i=1,10'
        source%lines(2)%chars = 'variable(i) = 1'
        source%lines(3)%chars = 'end do'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_do(source)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_forall() result (result_)
        use rules_m, only: indent_forall
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'FORALL (I=1:N)  A(I, I) = 1'
        source%lines(2)%chars = 'call thing()'
        source%lines(3)%chars = 'end forall'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_forall(source)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_if() result (result_)
        use rules_m, only: indent_if
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'if (condition) then'
        source%lines(2)%chars = 'call test'
        source%lines(3)%chars = 'end if'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_if(source)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_interface() result (result_)
        use rules_m, only: indent_interface
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'interface'
        source%lines(2)%chars = 'function foo()'
        source%lines(3)%chars = 'end interface'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_interface(source)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_function() result (result_)
        use rules_m, only: indent_function
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'function test'
        source%lines(2)%chars = 'implicit none'
        source%lines(3)%chars = 'end function test' !'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_function(source)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_labeled() result (result_)
        use rules_m, only: indent_do, indent_if, indent_forall, indent_associate, indent_block, &
                indent_select, indent_where, indent_case_clause
        type (result_t) :: result_
        type (SourceFile) :: source
        integer :: expected(25)

        expected = 0
        expected([2,5,8,11,14,17,20,24]) = 1
        expected(21) = 2

        source%nlines = 25
        allocate (source%lines(25))
        source%lines(1)%chars = 'label: do i=1,10'
        source%lines(2)%chars = 'variable(i) = 1'
        source%lines(3)%chars = 'end do'
        source%lines(4)%chars = '300 do i=1,10'
        source%lines(5)%chars = 'variable(i) = 1'
        source%lines(6)%chars = 'end do'
        source%lines(7)%chars = 'label_2: if (expression) then'
        source%lines(8)%chars = 'call thing()'
        source%lines(9)%chars = 'end if'
        source%lines(10)%chars = 'label_3: forall (i=1,10)'
        source%lines(11)%chars = 'thing()'
        source%lines(12)%chars = 'end forall'
        source%lines(13)%chars = 'label_4: associate '
        source%lines(14)%chars = 'thing()'
        source%lines(15)%chars = 'end associate'
        source%lines(16)%chars = 'label_5: block'
        source%lines(17)%chars = 'integer :: ew'
        source%lines(18)%chars = 'end block'
        source%lines(19)%chars = 'label_6: select case (r)'
        source%lines(20)%chars = 'case (3)'
        source%lines(21)%chars = 'thing()'
        source%lines(22)%chars = 'end select'
        source%lines(23)%chars = 'label_7: where (blah)'
        source%lines(24)%chars = 'thing()'
        source%lines(25)%chars = 'end where'
        allocate (source%indent_lvls(25))
        source%indent_lvls = 0

        call indent_do(source)
        call indent_if(source)
        call indent_forall(source)
        call indent_associate(source)
        call indent_block(source)
        call indent_select(source)
        call indent_case_clause(source)
        call indent_where(source)
        result_ = assert_that(all(source%indent_lvls==expected))
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_levels_identifies() result (result_)
        use rules_m, only: indent_levels
        use regex_m, only: start_program_exp, end_program_exp
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'program test'
        source%lines(2)%chars = 'implicit none'
        source%lines(3)%chars = 'end program test'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        source%indent_lvls = source%indent_lvls + indent_levels(source, start_program_exp, &
                end_program_exp)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_levels_multiple() result (result_)
        use rules_m, only: indent_levels
        use regex_m, only: start_program_exp, end_program_exp
        type (result_t) :: result_
        type (SourceFile) :: source
        logical :: checks(3)

        source%nlines = 7
        allocate (source%lines(7))
        source%lines(1)%chars = 'program test'
        source%lines(2)%chars = 'implicit none'
        source%lines(3)%chars = 'end program test'
        source%lines(4)%chars = ''
        source%lines(5)%chars = 'program test2'
        source%lines(6)%chars = 'implicit none'
        source%lines(7)%chars = 'end program test2'
        allocate (source%indent_lvls(7))
        source%indent_lvls = 0

        source%indent_lvls = source%indent_lvls + indent_levels(source, start_program_exp, &
                end_program_exp)
        if (source%indent_lvls(2)==1) checks(1) = .true.
        if (source%indent_lvls(5)==0) checks(2) = .true.
        if (source%indent_lvls(6)==1) checks(3) = .true.
        result_ = assert_that(all(checks))
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_levels_with_end() result (result_)
        use rules_m, only: indent_module
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'module test'
        source%lines(2)%chars = 'implicit none'
        source%lines(3)%chars = 'end'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_module(source)
        result_ = assert_that(source%indent_lvls(3)==0)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_module() result (result_)
        use rules_m, only: indent_module
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'module test'
        source%lines(2)%chars = 'implicit none'
        source%lines(3)%chars = 'end module test'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_module(source)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_prog() result (result_)
        use rules_m, only: indent_prog
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'program test'
        source%lines(2)%chars = 'implicit none'
        source%lines(3)%chars = 'end program test'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_prog(source)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_select() result (result_)
        use rules_m, only: indent_select
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'select case (foo)'
        source%lines(2)%chars = 'case (1)'
        source%lines(3)%chars = 'end select'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_select(source)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_submodule() result (result_)
        use rules_m, only: indent_submodule
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'submodule (test) test2'
        source%lines(2)%chars = 'implicit none'
        source%lines(3)%chars = 'end submodule test2'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_submodule(source)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_subroutine() result (result_)
        use rules_m, only: indent_subroutine
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'subroutine test()'
        source%lines(2)%chars = 'implicit none'
        source%lines(3)%chars = 'end subroutine test'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_subroutine(source)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_type() result (result_)
        use rules_m, only: indent_type
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 5
        allocate (source%lines(5))
        source%lines(1)%chars = 'type :: atype'
        source%lines(2)%chars = 'integer :: foo'
        source%lines(3)%chars = 'end type'
        source%lines(4)%chars = 'type (atype) thing'
        source%lines(5)%chars = 'type (atype) anotherthing'
        allocate (source%indent_lvls(5))
        source%indent_lvls = 0

        call indent_type(source)
        result_ = assert_that((source%indent_lvls(2)==1).and.&
                (source%indent_lvls(5)==0))
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_typeis_clause() result (result_)
        use rules_m, only: indent_typeis_clause
        type (result_t) :: result_
        type (SourceFile) :: source
        integer :: expected(6)

        expected = [0,0,1,0,0,0]
        source%nlines = 6
        allocate (source%lines(6))
        source%lines(1)%chars = 'select type (foo)'
        source%lines(2)%chars = 'type is (typea)'
        source%lines(3)%chars = 'statement'
        source%lines(4)%chars = 'class is (classa)'
        source%lines(5)%chars = 'statement'
        source%lines(6)%chars = 'end select'
        allocate (source%indent_lvls(6))
        source%indent_lvls = 0

        call indent_typeis_clause(source)
        result_ = assert_that(all(source%indent_lvls==expected))
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_indent_where() result (result_)
        use rules_m, only: indent_where
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'where (mask)'
        source%lines(2)%chars = 'call test'
        source%lines(3)%chars = 'end where'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        call indent_where(source)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_unindent_case() result (result_)
        use rules_m, only: unindent_case
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 4
        allocate (source%lines(4))
        source%lines(1)%chars = 'select case (foo)'
        source%lines(2)%chars = '   case (1)'
        source%lines(3)%chars = '   call do_thing()'
        source%lines(4)%chars = 'end case'
        allocate (source%indent_lvls(3))
        source%indent_lvls = [0,1,1,0]

        call unindent_case(source)
        result_ = assert_that(source%indent_lvls(2)==0)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_unindent_contains() result (result_)
        use rules_m, only: unindent_contains
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 4
        allocate (source%lines(4))
        source%lines(1)%chars = 'program'
        source%lines(2)%chars = '    implicit none'
        source%lines(3)%chars = '    contains'
        source%lines(4)%chars = 'end program'
        allocate (source%indent_lvls(3))
        source%indent_lvls = [0,1,1,0]

        call unindent_contains(source)
        result_ = assert_that(source%indent_lvls(3)==0)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_unindent_elsewhere() result (result_)
        use rules_m, only: unindent_elsewhere
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 5
        allocate (source%lines(5))
        source%lines(1)%chars = 'where (exp)'
        source%lines(2)%chars = '   statement'
        source%lines(3)%chars = '   elsewhere'
        source%lines(4)%chars = '   statement'
        source%lines(5)%chars = 'end if'
        allocate (source%indent_lvls(3))
        source%indent_lvls = [0,1,1,1,0]

        call unindent_elsewhere(source)
        result_ = assert_that(source%indent_lvls(3)==0)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_unindent_else() result (result_)
        use rules_m, only: unindent_else
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 5
        allocate (source%lines(5))
        source%lines(1)%chars = 'if (expression) then'
        source%lines(2)%chars = '   statement'
        source%lines(3)%chars = '   else'
        source%lines(4)%chars = '   statement'
        source%lines(5)%chars = 'end if'
        allocate (source%indent_lvls(3))
        source%indent_lvls = [0,1,1,1,0]

        call unindent_else(source)
        result_ = assert_that(source%indent_lvls(3)==0)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_unindent_statement() result (result_)
        use rules_m, only: unindent_statement
        use regex_m, only: contains_exp
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 4
        allocate (source%lines(4))
        source%lines(1)%chars = 'program'
        source%lines(2)%chars = '    implicit none'
        source%lines(3)%chars = '    contains'
        source%lines(4)%chars = 'end program'
        allocate (source%indent_lvls(3))
        source%indent_lvls = [0,1,1,0]

        source%indent_lvls = source%indent_lvls + unindent_statement(source, contains_exp)
        result_ = assert_that(source%indent_lvls(3)==0)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_rules_list_associate() result (result_)
        use rules_m, only: rule_list_t, indent_do
        type (result_t) :: result_
        type (rule_list_t) :: rules
        type (SourceFile) :: source

        source%nlines = 3
        allocate (source%lines(3))
        source%lines(1)%chars = 'do i=1,19'
        source%lines(2)%chars = 'a=1'
        source%lines(3)%chars = 'end do'
        allocate (source%indent_lvls(3))
        source%indent_lvls = 0

        rules%routine => indent_do
        call rules%routine(source)
        result_ = assert_that(source%indent_lvls(2)==1)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


    function check_single_if() result (result_)
        use rules_m, only: indent_if
        type (result_t) :: result_
        type (SourceFile) :: source

        source%nlines = 2
        allocate (source%lines(2))
        source%lines(1)%chars = 'if (condition) call test'
        source%lines(2)%chars = 'call test'
        allocate (source%indent_lvls(2))
        source%indent_lvls = 0

        call indent_if(source)
        result_ = assert_that(source%indent_lvls(2)==0)
        deallocate (source%lines)
        deallocate (source%indent_lvls)
    end function


end module indent_rules_test
