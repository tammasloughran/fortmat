module hspace_rules_test
    use sourcefile_m, only: SourceFile
    use vegetables
    implicit none

contains


    function test_hspace_rules() result (tests)
        type (test_item_t) :: tests

        tests = describe("hspace rules", [ &
                it("hspace_allocate puts space between allocate and (", check_hspace_allocate_par), &
                it("hspace_associate_par puts space between associate and (", check_hspace_associate_par), &
                it("hspace_case_par puts space between case and (", check_hspace_case_par), &
                it("hspace_character_par puts space between character and (", check_hspace_character_par), &
                it("hspace_class_par puts space between class and (", check_hspace_class_par), &
                it("hspace_classis_par puts space between classis and (", check_hspace_classis_par), &
                it("hspace_close_par puts space between close and (", check_hspace_close_par), &
                it("hspace_complex_par puts space between complex and (", check_hspace_complex_par), &
                it("hspace_deallocate_par puts space between deallocate and (", check_hspace_deallocate_par), &
                it("hspace_dimension_par puts space between dimension and (", check_hspace_dimension_par), &
                it("hspace_do_while puts space between do and while", check_hspace_do_while), &
                it("hspace_dowhile_par puts space between dowhile and (", check_hspace_dowhile_par), &
                it("hspace_else_if puts space between else and if", check_hspace_else_if), &
                it("hspace_elseif_par puts space between elseif and (", check_hspace_elseif_par), &
                it("hspace_end_associate puts space between end and associate", check_hspace_end_associate), &
                it("hspace_end_block puts space between end and block", check_hspace_end_block), &
                it("hspace_end_do puts space between end and do", check_hspace_end_do), &
                it("hspace_end_forall puts space between end and forall", check_hspace_end_forall), &
                it("hspace_end_function puts space between end and function", check_hspace_end_function), &
                it("hspace_end_if puts space between end and if", check_hspace_end_if), &
                it("hspace_end_interface puts space between end and interface", check_hspace_end_interface), &
                it("hspace_end_module puts space between end and module", check_hspace_end_module), &
                it("hspace_end_program puts space between end and program", check_hspace_end_program), &
                it("hspace_end_select puts space between end and select", check_hspace_end_select), &
                it("hspace_end_submodule puts space between end and submodule", check_hspace_end_submodule), &
                it("hspace_end_subroutine puts space between end and subroutine", check_hspace_end_subroutine), &
                it("hspace_end_where puts space between end and where", check_hspace_end_where), &
                it("hspace_forall_par puts space between forall and (", check_hspace_forall_par), &
                it("hspace_if_par puts space between if and (", check_hspace_if_par), &
                it("hspace_integer_par puts space between integer and (", check_hspace_integer_par), &
                it("hspace_intent_par puts space between intent and (", check_hspace_intent_par), &
                it("hspace_logical_par puts space between logical and (", check_hspace_logical_par), &
                it("hspace_open_par puts space between open and (", check_hspace_open_par), &
                it("hspace_par_then puts space between ) and then", check_hspace_par_then), &
                it("hspace_print_star puts space between print and *", check_hspace_print_star), &
                it("hspace_real_par puts space between real and (", check_hspace_real_par), &
                it("hspace_result_par puts space between result and (", check_hspace_result_par), &
                it("hspace_rewind_par puts space between rewind and (", check_hspace_rewind_par), &
                it("hspace_separator puts spaces around ::", check_hspace_separator), &
                it("hspace_type_par puts space between type and (", check_hspace_type_par), &
                it("hspace_typeis_par puts space between typeis and (", check_hspace_typeis_par), &
                it("hspace_where_par puts space between where and (", check_hspace_where_par), &
                it("hspace_write_par puts space between write and (", check_hspace_write_par) ])
    end function


    function check_hspace_separator() result (result_)
        use rules_m, only: hspace_separator
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=14) :: expected = 'integer :: foo'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'integer::foo'

        call hspace_separator(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_class_par() result (result_)
        use rules_m, only: hspace_class_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=7) :: expected = 'class ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'class('

        call hspace_class_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_dimension_par() result (result_)
        use rules_m, only: hspace_dimension_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=11) :: expected = 'dimension ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'dimension('

        call hspace_dimension_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_do_while() result (result_)
        use rules_m, only: hspace_do_while
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=8) :: expected = 'do while'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'dowhile'

        call hspace_do_while(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_dowhile_par() result (result_)
        use rules_m, only: hspace_dowhile_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=10) :: expected = 'do while ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'do while('

        call hspace_dowhile_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_else_if() result (result_)
        use rules_m, only: hspace_else_if
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=7) :: expected = 'else if'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'elseif'

        call hspace_else_if(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_elseif_par() result (result_)
        use rules_m, only: hspace_elseif_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=9) :: expected = 'else if ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'else if('

        call hspace_elseif_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_allocate_par() result (result_)
        use rules_m, only: hspace_allocate_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=10) :: expected = 'allocate ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'allocate('

        call hspace_allocate_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_deallocate_par() result (result_)
        use rules_m, only: hspace_deallocate_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=12) :: expected = 'deallocate ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'deallocate('

        call hspace_deallocate_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_par_then() result (result_)
        use rules_m, only: hspace_par_then
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=14) :: expected = 'if(thing) then'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'if(thing)then'

        call hspace_par_then(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_print_star() result (result_)
        use rules_m, only: hspace_print_star
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=12) :: expected = 'print *, foo'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'print*, foo'

        call hspace_print_star(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_result_par() result (result_)
        use rules_m, only: hspace_result_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=8) :: expected = 'result ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'result('

        call hspace_result_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_end_do() result (result_)
        use rules_m, only: hspace_end_do
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=6) :: expected = 'end do'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'enddo'

        call hspace_end_do(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_end_program() result (result_)
        use rules_m, only: hspace_end_program
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=11) :: expected = 'end program'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'endprogram'

        call hspace_end_program(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_end_if() result (result_)
        use rules_m, only: hspace_end_if
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=6) :: expected = 'end if'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'endif'

        call hspace_end_if(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_end_module() result (result_)
        use rules_m, only: hspace_end_module
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=10) :: expected = 'end module'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'endmodule'

        call hspace_end_module(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_end_function() result (result_)
        use rules_m, only: hspace_end_function
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=12) :: expected = 'end function'!'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'endfunction'

        call hspace_end_function(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_end_subroutine() result (result_)
        use rules_m, only: hspace_end_subroutine
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=14) :: expected = 'end subroutine'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'endsubroutine'

        call hspace_end_subroutine(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_end_select() result (result_)
        use rules_m, only: hspace_end_select
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=10) :: expected = 'end select'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'endselect'

        call hspace_end_select(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_end_interface() result (result_)
        use rules_m, only: hspace_end_interface
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=13) :: expected = 'end interface'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'endinterface'

        call hspace_end_interface(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_end_where() result (result_)
        use rules_m, only: hspace_end_where
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=9) :: expected = 'end where'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'endwhere'

        call hspace_end_where(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_end_submodule() result (result_)
        use rules_m, only: hspace_end_submodule
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=13) :: expected = 'end submodule'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'endsubmodule'

        call hspace_end_submodule(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_end_block() result (result_)
        use rules_m, only: hspace_end_block
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=9) :: expected = 'end block'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'endblock'

        call hspace_end_block(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_end_forall() result (result_)
        use rules_m, only: hspace_end_forall
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=10) :: expected = 'end forall'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'endforall'

        call hspace_end_forall(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_end_associate() result (result_)
        use rules_m, only: hspace_end_associate
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=13) :: expected = 'end associate'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'endassociate'

        call hspace_end_associate(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function


    function check_hspace_write_par() result (result_)
        use rules_m, only: hspace_write_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=18) :: expected = 'write (*,*) string'

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'write(*,*) string'

        call hspace_write_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_write_par


    function check_hspace_associate_par() result (result_)
        use rules_m, only: hspace_associate_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=11) :: expected = 'associate ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'associate('

        call hspace_associate_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_associate_par


    function check_hspace_case_par() result (result_)
        use rules_m, only: hspace_case_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=6) :: expected = 'case ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'case('

        call hspace_case_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_case_par


    function check_hspace_character_par() result (result_)
        use rules_m, only: hspace_character_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=11) :: expected = 'character ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'character('

        call hspace_character_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_character_par


    function check_hspace_close_par() result (result_)
        use rules_m, only: hspace_close_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=7) :: expected = 'close ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'close('

        call hspace_close_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_close_par


    function check_hspace_forall_par() result (result_)
        use rules_m, only: hspace_forall_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=8) :: expected = 'forall ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'forall('

        call hspace_forall_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_forall_par


    function check_hspace_if_par() result (result_)
        use rules_m, only: hspace_if_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=4) :: expected = 'if ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'if('

        call hspace_if_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_if_par


    function check_hspace_intent_par() result (result_)
        use rules_m, only: hspace_intent_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=8) :: expected = 'intent ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'intent('

        call hspace_intent_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_intent_par


    function check_hspace_integer_par() result (result_)
        use rules_m, only: hspace_integer_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=9) :: expected = 'integer ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'integer('

        call hspace_integer_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_integer_par


    function check_hspace_classis_par() result (result_)
        use rules_m, only: hspace_classis_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=9) :: expected = 'classis ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'classis('

        call hspace_classis_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_classis_par


    function check_hspace_typeis_par() result (result_)
        use rules_m, only: hspace_typeis_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=9) :: expected = 'type is ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'type is('

        call hspace_typeis_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_typeis_par


    function check_hspace_logical_par() result (result_)
        use rules_m, only: hspace_logical_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=9) :: expected = 'logical ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'logical('

        call hspace_logical_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_logical_par


    function check_hspace_open_par() result (result_)
        use rules_m, only: hspace_open_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=6) :: expected = 'open ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'open('

        call hspace_open_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_open_par


    function check_hspace_real_par() result (result_)
        use rules_m, only: hspace_real_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=6) :: expected = 'real ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'real('

        call hspace_real_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_real_par


    function check_hspace_rewind_par() result (result_)
        use rules_m, only: hspace_rewind_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=8) :: expected = 'rewind ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'rewind('

        call hspace_rewind_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_rewind_par


    function check_hspace_type_par() result (result_)
        use rules_m, only: hspace_type_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=6) :: expected = 'type ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'type('

        call hspace_type_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_type_par


    function check_hspace_where_par() result (result_)
        use rules_m, only: hspace_where_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=7) :: expected = 'where ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'where('

        call hspace_where_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_where_par


    function check_hspace_complex_par() result (result_)
        use rules_m, only: hspace_complex_par
        type (result_t) :: result_
        type (SourceFile) :: source
        character (len=9) :: expected = 'complex ('

        source%nlines = 1
        allocate (source%lines(1))
        source%lines(1)%chars = 'complex('

        call hspace_complex_par(source)
        result_ = assert_that(source%lines(1)%chars==expected)

        deallocate (source%lines)
    end function check_hspace_complex_par


end module hspace_rules_test
