module cli_test
    use vegetables
    implicit none
    private

    public :: test_cli

contains

    function test_cli() result (tests)
        type (test_item_t) :: tests

        tests = describe("parse_cli", [ &
                it("parses the -p argument", check_parse_cli_parses_p), &
                it("parses the --style-spec argument", check_parse_cli_parses_style_spec), &
                it("parses the --output-spec argument", check_parse_cli_parses_output_spec)])!, &
                !it("parses source files", check_parse_cli_parses_source_files)])
    end function test_cli

    function check_parse_cli_parses_p() result (result_)
        use cli_m, only: print_to_stdout, parse_cli
        type (result_t) :: result_

        call parse_cli()
        result_ = assert_that(print_to_stdout.eqv..false.)
    end function check_parse_cli_parses_p

    function check_parse_cli_parses_style_spec() result (result_)
        use cli_m, only: style_spec_file, parse_cli
        type (result_t) :: result_

        call parse_cli()
        result_ = assert_equals(style_spec_file, ' ')
    end function check_parse_cli_parses_style_spec

    function check_parse_cli_parses_output_spec() result (result_)
        use cli_m, only: output_spec, parse_cli
        type (result_t) :: result_

        call parse_cli()
        result_ = assert_that(output_spec.eqv..false.)
    end function check_parse_cli_parses_output_spec
end module cli_test

