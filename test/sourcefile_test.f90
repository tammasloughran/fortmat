module sourcefile_test
    use sourcefile_m, only: SourceFile
    use vegetables
    implicit none

contains

    function test_sourcefile() result (tests)
        type (test_item_t) :: tests

        tests = describe("SourceFile", [ &
                it("constructor reads a file", check_sourcefile_reads_file), &
                it("constructor lines match", check_sourcefile_lines_match), &
                it("constructor indent_lvls match", check_sourcefile_indent_lvls_match), &
                it("count_lines counts lines of a file", check_count_lines) ])
    end function test_sourcefile

    function check_sourcefile_reads_file() result (result_)
        type (result_t) :: result_
        type (SourceFile) :: source

        source = SourceFile('test/testfile')
        close (source%iounit)
        result_ = assert_that(source%file_name=='test/testfile')
    end function check_sourcefile_reads_file

    function check_sourcefile_lines_match() result (result_)
        type (result_t) :: result_
        type (SourceFile) :: source

        source = SourceFile('test/testfile')
        close (source%iounit)
        result_ = assert_that(size(source%lines)==source%nlines)
    end function check_sourcefile_lines_match

    function check_sourcefile_indent_lvls_match() result (result_)
        type (result_t) :: result_
        type (SourceFile) :: source

        source = SourceFile('test/testfile')
        close (source%iounit)
        result_ = assert_that(size(source%lines)==size(source%indent_lvls))        
    end function check_sourcefile_indent_lvls_match

    function check_count_lines() result (result_)
        use sourcefile_m, only: count_lines
        type (result_t) :: result_
        integer :: iounit
        
        open (newunit=iounit, file='test/testfile')
        result_ = assert_that(count_lines(iounit)==8)
        close (iounit)
    end function check_count_lines

end module sourcefile_test
