program fortmat
    !! Main program for the fortmat source code formatter.
    use, intrinsic :: iso_fortran_env, only: output_unit
    use cli_m, only: parse_cli, filenames, print_to_stdout, output_spec
    use apply_rules_m, only: apply_rules_to_file
    use sourcefile_m, only: SourceFile
    use style_spec_m, only: construct_style_spec
    use rules_m, only: construct_rules_list, rule_list_t
    implicit none
    character (len=:), allocatable :: filename
    integer :: fnum
    integer :: i
    integer :: out_unit
    type (SourceFile) :: source
    type (rule_list_t), allocatable :: rules_to_run(:)

    call parse_cli()

    ! Read the namelist file and generate the style specification.
    call construct_style_spec()

    if ((size(filenames)==0).and.(.not.output_spec)) then
        error stop "No input files."
    else if ((size(filenames)==0).and.(output_spec)) then
        print *, "Output default specification to default_spec.nml"
        stop
    end if

    rules_to_run = construct_rules_list()

    ! Main loop over input files.
    do fnum=1,size(filenames)
        filename = filenames(fnum)
        source = SourceFile(filename)
        call apply_rules_to_file(rules_to_run, source)
        if (print_to_stdout) then
            print *, " "
            print '(2A)', "!File: ", trim(filenames(fnum))
            out_unit = output_unit
        else
            out_unit = source%iounit
            rewind (out_unit)
        end if

        do i=1,source%nlines
            write (unit=out_unit, fmt='(A)') trim(source%lines(i)%chars)
        end do
    end do
end program fortmat

! Copyright (C) 2021  Tammas Loughran
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
