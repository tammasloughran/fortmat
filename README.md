# Fortmat

Fortmat is a Fortran source code formatter with customizable styles.

Detailed programmers documentation can be found here: https://tammasloughran.gitlab.io/fortmat/

## Build

You will need an installation of the PCRE library. For example, on Ubuntu, this can be installed
using apt:

```bash
sudo apt install libpcre3-dev
```

You will also need a fortran 2008 compiler. gfortran works well.

```bash
sudo apt install gfortran
```

### fpm

You can use [fpm](https://fpm.fortran-lang.org/) to build fortmat.

```bash
git clone https://gitlab.com/tammasloughran/fortmat.git
cd fortmat
fpm build
```

### Make

If fpm is not available on your system, then you can use make to build it.

```bash
git clone https://gitlab.com/tammasloughran/fortmat.git
cd fortmat
make
```
The executable should be in bin/Debug/

## Use

```text
Usage:
    fortmat [options] <file>...

A fortran source code formatter.

Options:
 -p               Print formatted text to stdout instead of overwriting input
                      files.
 -s --style-spec  Namelist file containing the style specification.
 --output-spec    Generate the default style specification namelist.
 --help           Display this help.
 --version        Display version.
```

## Custom style

Do you not like the default style? Good. You can change it with a namelist file. Create the default
style specification:

```bash
fortmat --output-spec
```

This generates the file default_spec.nml, which should be modified to suit your style.
Then you can use it witht he -s option.
```bash
fortmat -p -s my_style.nml source_file.f90
```

