---
project: tammasloughran/fortmat
summary: ![Fortmat](|media|/icon.png)<br>
         A Fortran source code formatter
src_dir: ../src
         ../app
exclude_dir: src/tests
media_dir: ../doc
output_dir: ford_docs
project_github: https://gitlab.com/tammasloughran/fortmat
author: Tammas Loughran
github: https://gitlab.com/tammasloughran
email: tamloughran@outlook.com
source: false
graph: true
search: true
fpp_extensions: fypp
preprocess: true
macro: MAXRANK=3
preprocessor: fypp
display: public
         protected
proc_internals: true
graph_maxnodes: 250
graph_maxdepth: 5
favicon: icon.png
coloured_edges: true
sort: permission-alpha
extra_mods: iso_fortran_env:https://gcc.gnu.org/onlinedocs/gfortran/ISO_005fFORTRAN_005fENV.html
            iso_c_binding:https://gcc.gnu.org/onlinedocs/gfortran/ISO_005fC_005fBINDING.html#ISO_005fC_005fBINDING
            M_CLI2:https://urbanjost.github.io/M_CLI2/M_CLI2.3m_cli2.html
print_creation_date: true
creation_date: %Y-%m-%d %H:%M %z
dbg: true

---

Fortmat is a Fortran source code formatter with customizable styles.

@warning This project is a very early work in progress (It has only limited functionality). This
documentation may not be up to date.

## Build

You will need an installation of the PCRE library. For example, on Ubuntu, this can be installed
using apt:

```bash
sudo apt install libpcre3-dev
```

### fpm

You can use [fpm](https://fpm.fortran-lang.org/) to build fortmat.

```bash
git clone https://gitlab.com/tammasloughran/fortmat.git
cd fortmat
fpm build
```

### Make

If fpm is not available on your system, then you can use make to build it.

```bash
git clone https://gitlab.com/tammasloughran/fortmat.git
cd fortmat
make
```
The executable should be in bin/Debug/

## Use

```text
Usage:
    fortmat [options] <file>...

A fortran source code formatter.

Options:
 -p               Print formatted text to stdout instead of overwriting input
                      files.
 -s --style-spec  Namelist file containing the style specification.
 --output-spec    Generate the default style specification namelist.
 --help           Display this help.
 --version        Display version.
```

## Custom style

Do you not like the default style? Good. You can change it with a namelist file. Create the default
style specification:

```bash
fortmat --output-spec
```

This generates the file default_spec.nml, which should be modified to suit your style.
Then you can use it witht he -s option.
```bash
fortmat -p -s my_style.nml source_file.f90
```

